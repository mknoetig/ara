""" high level plotting program, creates theta^2 plot and spectra 
    for a given timerange, source, etc..

Usage:
  iterate_over_selected_nights_filling_usefulness_map.py <source> <threshold_max> <zenith_min> <zenith_max> <fNight_min> <fNight_max>
  iterate_over_selected_nights_filling_usefulness_map.py (-h | --help)
  iterate_over_selected_nights_filling_usefulness_map.py --version

Options:
  -h --help     Show this screen.
  --version     Show version.
"""
from __future__ import absolute_import, print_function, division

import os
import sys
import pickle

import numpy as np

from tqdm import tqdm
from docopt import docopt
from sql import *
import webbrowser
from time import sleep
import csv


from sources import *
from contextlib import contextmanager

@contextmanager
def suppress_stdout():
    with open(os.devnull, "w") as devnull:
        old_stdout = sys.stdout
        sys.stdout = devnull
        try:  
            yield
        finally:
            sys.stdout = old_stdout

if __name__ == "__main__":

    #read config and initialize program
    arguments = docopt(__doc__, version='Ara 1')
    runinfo = get_query(standard_query_pass0_2)
    fNights_data = []
    for row in runinfo:
        fNights_data.append(row)
    fNights_data.sort()
    list_of_nights = list(set([ row[0] for row in fNights_data]))
    list_of_nights.sort()

    list_of_starting_times = []
    last_night = 0
    for i in range(len(fNights_data)):
        if fNights_data[i][0] != last_night:
            list_of_starting_times.append(fNights_data[i][2])
        last_night=fNights_data[i][0]

    list_of_perfect_nights = []
    list_of_crap_nights = []
    list_of_undecided_nights = []
    iterator = 0

    while True:
        night = list_of_nights[iterator]
        buf = "%d"%night
        night_string = buf[:4] + '-' + buf[4:6] + '-' + buf[6:8]
        night_string2 = buf[:4] + '/' + buf[4:6] + '/' + buf[6:8]
        starting_string = list_of_starting_times[iterator].strftime("%H:%M")

        with suppress_stdout():
            webbrowser.open('http://www.fact-project.org/dch/quality.php?date='+night_string)
        
        with suppress_stdout():
            webbrowser.open('http://fact-project.org/gtc_images/'+night_string2)

        #print all runs in that nightprint()
        print("fNIGHT fRunId Start Stop")
        for row in fNights_data:
            if row[0] == night:
                print(row[0],row[1],row[2].strftime("%H:%M"),row[3].strftime("%H:%M"))
        fact_rating = input("fNight: %s start: %s "%(night_string,starting_string))
        iterator = iterator +1

        if iterator==len(list_of_nights):
            break



    