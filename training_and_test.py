import itertools 
import numpy as np
import pandas as pd

import sklearn
from sklearn_pandas import DataFrameMapper
from sklearn.ensemble import RandomForestRegressor,RandomForestClassifier
from sklearn.decomposition import PCA
from sklearn.grid_search import GridSearchCV

# ----- globals  -------

cut_map = {}

# --- functions ---

def define_test_and_training_set( df, cfg ):
    test_ratio = cfg['test_to_train_ratio']
    train_mask = np.random.choice([False, True], len(df), 
        p=[test_ratio/(1.+test_ratio), 1./(1+test_ratio)])
    test_mask = (train_mask == 0)

    df['training set'] = train_mask
    df['testing set'] = test_mask

def apply_default_mc_missmatch_cuts( df, cfg ):

    mask_list = np.ones(len(df), dtype=bool)
    for i in cfg['analysis_cuts'][cfg['analysis_cuts']['name_of_chosen_cut']]:
        mask_list = mask_list&df[i[0]].apply(eval(i[1]))
    
    return df[mask_list].copy()

def update_survived_cut_column( corsika_df, surviving_df, number_of_cut, name_of_cut, cfg):
    if name_of_cut in cut_map:
        raise Exception("this cut name is already taken")

    # hacky way of intialition of this column
    if len(cut_map) == 0:
        corsika_df["survived_cut"] = np.int16(0)

    cut_map[name_of_cut] = number_of_cut
    corsika_df.loc[surviving_df.index, "survived_cut"] = number_of_cut

def get_pandas_to_sklearn_mapping( df, name_in_config, cfg ):
    mapper_list = []
    for key_tuple in cfg[name_in_config]['keys_for_training']:
        key_buf = list(key_tuple)
        if key_buf[1] is not None:
            key_buf[1] = [ eval(transformation) for transformation in key_buf[1] ]
        mapper_list.append( tuple(key_buf) )

    mapper = DataFrameMapper(mapper_list)
    mapper.fit(df.copy())
    return mapper

def apply_pandas_to_sklearn_mapping( df, mapper ):
    np_array_buf = mapper.transform(df.copy())
    return np_array_buf

def get_trained_classifier_or_regressor(train_data,train_labels,train_type, cfg):
    classifier = eval(cfg[train_type]['method'])

    clf = GridSearchCV(classifier, cfg[train_type]['tuned_parameters'], 
        cv=cfg[train_type]['crossvalidation'], 
        scoring=cfg[train_type]['score'])
    blub = clf.fit(train_data, train_labels)

    print("Grid "+cfg[train_type]['score']+" scores on development set:")
    for params, mean_score, scores in clf.grid_scores_:
       print("%0.3f (+/-%0.03f) in %s" % (mean_score, scores.std() / 2, params))

    #return the energy regressor grid search and the indixes used in testing
    return clf

def train_and_apply_disp_regressor( df, cfg ):
    # make a list of particle_id masks
    pid_mask = df['particle id (particle code or A x 100 + Z for nuclei)'] == 1
    
    train_mask = df['training set'] 

    df['ARA_disp_regressor']=np.zeros(len(df),dtype="float32")
    
    #get a set of masks for training and testing, only one for testing as we always apply the regressors to all rows
    pid_train_masks = (pid_mask&train_mask)

    print("Building ARA disp regressor")
    
    #fist get mapper
    sklearn_pd_mapper = get_pandas_to_sklearn_mapping( df[ pid_train_masks ],'disp_regressor', cfg )

    #then train
    disp_train_np_array_buf = apply_pandas_to_sklearn_mapping( df[ pid_train_masks ], sklearn_pd_mapper ) 

    disp_train_data = disp_train_np_array_buf[:,:-1] # the data, comes before the target column
    disp_train_target = disp_train_np_array_buf[:,-1] # target has to be in the last column
    disp_regr = get_trained_classifier_or_regressor(disp_train_data,disp_train_target,'disp_regressor',cfg)
    
    #then test and get the indices for filling into the df
    disp_all_np_array_buf = apply_pandas_to_sklearn_mapping( df,sklearn_pd_mapper )
    index_buffer = pd.MultiIndex.from_tuples( df.index )

    #estimate the disp 
    predictions = disp_regr.predict( disp_all_np_array_buf[:,:-1] )
    predictions = np.array(predictions)
    
    df_buf = pd.DataFrame({'ARA_disp_regressor' : pd.Series(predictions, index=index_buffer)})

    df.loc[(df_buf.index),'ARA_disp_regressor'] = df_buf['ARA_disp_regressor']
    
    #save the disp into a pmml file
    from sklearn2pmml import sklearn2pmml
    sklearn2pmml(disp_regr.best_estimator_, sklearn_pd_mapper, "DispRegressor.pmml")

    return disp_regr.best_estimator_, sklearn_pd_mapper

def train_and_apply_energy_regressors( df, cfg ):
    # make a list of particle_id masks
    pid_set = set(df['particle id (particle code or A x 100 + Z for nuclei)'])
    pid_masks = [ df['particle id (particle code or A x 100 + Z for nuclei)'] == i  for i in pid_set ]
    
    train_mask = df['training set'] 
    
    for pid in pid_set:
        df['ARA_pid_%d_mean_Energy_Prediction_in_GeV' % pid]=np.zeros(len(df),dtype="float32")
        df['ARA_pid_%d_stdev_Energy_Prediction_in_GeV' % pid]=np.zeros(len(df),dtype="float32")

    #get a set of masks for training and testing, only one for testing as we always apply the regressors to all rows
    pid_train_masks = [ pid_mask&train_mask for pid_mask in pid_masks ]
    
    energy_regressors = []
    sklearn_pd_mappers = []
    
    for i,pid in enumerate(pid_set):
        print("Building energy regressor for particle pid: %d" % (pid))


        #first get mapper then train   
        trainmask = pid_train_masks[i]
        sklearn_pd_mapper = get_pandas_to_sklearn_mapping( df[ trainmask ],'energy_regressor', cfg)
        energy_train_np_array_buf = apply_pandas_to_sklearn_mapping( df[ trainmask ],sklearn_pd_mapper ) 
        sklearn_pd_mappers.append(sklearn_pd_mapper)


        energy_train_data = energy_train_np_array_buf[:,1:] # the data, comes after the target column
        energy_train_target = energy_train_np_array_buf[:,0] # target has to be in the first column
        energy_regr = get_trained_classifier_or_regressor(energy_train_data,energy_train_target,'energy_regressor',cfg)
        energy_regressors.append( energy_regr )

        #then test and get the indices for filling into the df
        energy_all_np_array_buf = apply_pandas_to_sklearn_mapping( df,sklearn_pd_mapper )
        index_buffer = pd.MultiIndex.from_tuples( df.index )

        #estimate the energy with all predictors, then insert mean and variance into the og. dataframe
        predictions = []
        for est in energy_regr.best_estimator_.estimators_:
            predictions.append(est.predict( energy_all_np_array_buf[:,1:] ))
        predictions = np.array(predictions)
        
        energy_buf_mean = np.median(predictions, axis = 0)
        energy_buf_stdev = np.sqrt(np.var(predictions, axis = 0))
        
        df_buf = pd.DataFrame(
            {('ARA_pid_%d_mean_Energy_Prediction_in_GeV' % pid) : pd.Series(energy_buf_mean, index=index_buffer),
            ('ARA_pid_%d_stdev_Energy_Prediction_in_GeV' % pid) : pd.Series(energy_buf_stdev, index=index_buffer)
            })

        df.loc[(df_buf.index),('ARA_pid_%d_mean_Energy_Prediction_in_GeV' % pid)] = df_buf[('ARA_pid_%d_mean_Energy_Prediction_in_GeV' % pid)]
        df.loc[(df_buf.index),('ARA_pid_%d_stdev_Energy_Prediction_in_GeV' % pid)] = df_buf[('ARA_pid_%d_stdev_Energy_Prediction_in_GeV' % pid)]

    return energy_regressors,sklearn_pd_mappers, pid_set

def apply_energy_regressors_to_data( df, eregr_s, emapper_s, pid_set ):

    if len(eregr_s) is not len(emapper_s):
        print("unequal legths: eregr_s != emapper_s")
        return 
    if len(eregr_s) is not len(pid_set):
        print("unequal legths: eregr_s != pid_set")
        return 
    
    df["total energy in GeV"]=-1

    for i,pid in enumerate(pid_set):

        energy_all_np_array_buf = apply_pandas_to_sklearn_mapping( df,emapper_s[i] )
        index_buffer = pd.MultiIndex.from_tuples( df.index )

        #estimate the energy with all predictors, then insert mean and variance into the og. dataframe
        predictions = []
        for est in eregr_s[i].best_estimator_.estimators_:
            predictions.append(est.predict( energy_all_np_array_buf[:,1:] ))
        predictions = np.array(predictions)
        
        energy_buf_mean = np.median(predictions, axis = 0)
        energy_buf_stdev = np.sqrt(np.var(predictions, axis = 0))
        
        df_buf = pd.DataFrame(
            {('ARA_pid_%d_mean_Energy_Prediction_in_GeV' % pid) : pd.Series(energy_buf_mean, index=index_buffer),
            ('ARA_pid_%d_stdev_Energy_Prediction_in_GeV' % pid) : pd.Series(energy_buf_stdev, index=index_buffer)
            })

        df.loc[(df_buf.index),('ARA_pid_%d_mean_Energy_Prediction_in_GeV' % pid)] = df_buf[('ARA_pid_%d_mean_Energy_Prediction_in_GeV' % pid)]
        df.loc[(df_buf.index),('ARA_pid_%d_stdev_Energy_Prediction_in_GeV' % pid)] = df_buf[('ARA_pid_%d_stdev_Energy_Prediction_in_GeV' % pid)]

    df = df.drop('total energy in GeV', 1)

    return df

def get_balance_mask( df, pid_combo, cfg ):
    n_pid_0 = sum(df['particle id (particle code or A x 100 + Z for nuclei)']==pid_combo[0])
    print("%d entries in " % n_pid_0,end="")
    print("pid %d classif. set" % pid_combo[0])
    n_pid_1 = sum(df['particle id (particle code or A x 100 + Z for nuclei)']==pid_combo[1])
    print("%d entries in " % n_pid_1,end="")
    print("pid %d classif. set" % pid_combo[1])

    #save which one is smaller and if both have an equal number, return true
    #smaller = 1
    if n_pid_0 < n_pid_1:
        print("pid %d smaller set than pid %d in classif. set, downsampling." % pid_combo)
        smaller = 0
        bigger = 1
        nsmaller = n_pid_0
    if n_pid_0 > n_pid_1:
        print("pid %d larger set than pid %d in classif. set, downsampling." % pid_combo)
        smaller = 1
        bigger = 0
        n_smaller = n_pid_1
    elif n_pid_0 == n_pid_1:
        print("pids %d and %d in equal numbers in classif. set, continue without downsampling." % pid_combo)
        return True

    #save all entries with the smaller set
    smaller_balance_mask = df['particle id (particle code or A x 100 + Z for nuclei)']==pid_combo[smaller]
    bigger_balance_mask = df['particle id (particle code or A x 100 + Z for nuclei)']==pid_combo[bigger]

    #reduce the entries with the larger set
    downsample = df[(df['particle id (particle code or A x 100 + Z for nuclei)']==pid_combo[bigger])].sample(n_smaller)
    downsampling_mask = np.in1d(df.index.values,downsample.index.values)
    
    balance_mask = downsampling_mask|smaller_balance_mask

    n_pid_0 = sum(df[balance_mask]['particle id (particle code or A x 100 + Z for nuclei)']==pid_combo[0])
    print("downsampled to %d entries in " % n_pid_0,end="")
    print("pid %d classif. set" % pid_combo[0])
    n_pid_1 = sum(df[balance_mask]['particle id (particle code or A x 100 + Z for nuclei)']==pid_combo[1])
    print("downsampled to %d entries in " % n_pid_1,end="")
    print("pid %d classif. set" % pid_combo[1])

    return balance_mask

def train_and_apply_classifiers( df, cfg ):
    pid_set = set(df['particle id (particle code or A x 100 + Z for nuclei)'])
    
    train_mask = df['training set'] 
    
    #make a list of possible 2 out-of N(pid) combinations for classification
    pid_combinations = [i for i in itertools.combinations(pid_set,2)]
    classifier_s = []
    pca_s = []
    sklearn_pd_mappers = []

    for pid_combo in pid_combinations:
    #pid_combo = pid_combinations[0]
        print("Building classifier: %d-to-%d-ness" % (pid_combo))

        # generate new columns in the df for the classifiers and default them nan
        df['ARA_pid_%d_to_%d_ness' % pid_combo]=np.empty(len(df),dtype="float32")
        df['ARA_pid_%d_to_%d_ness' % pid_combo]=np.NAN

        # generate temporary masks so that the respective pids are taken into account equally
        pid_mask = (df['particle id (particle code or A x 100 + Z for nuclei)']==pid_combo[0])|(df['particle id (particle code or A x 100 + Z for nuclei)']==pid_combo[1])

        # get a mask such that the pids are equally represented ( e.g. 50k protons, 50k gammas )
        balance_mask = get_balance_mask( df[ pid_mask&train_mask ] , pid_combo, cfg ) 
        pid_train_data_masks = ( pid_mask&train_mask&balance_mask )

        #fist get mapper
        sklearn_pd_mapper = get_pandas_to_sklearn_mapping( df[ pid_train_data_masks ],'classification',cfg )
        sklearn_pd_mappers.append(sklearn_pd_mapper)

        #then train
        classification_np_array_buf = apply_pandas_to_sklearn_mapping( df[ pid_train_data_masks ],sklearn_pd_mapper )
        index_buffer = pd.MultiIndex.from_tuples( df.index )

        train_data = classification_np_array_buf[:,1:] # the data, comes after the target column
        train_labels = classification_np_array_buf[:,0].astype(int) # target has to be in the first column, pass it as string
        train_labels[train_labels==pid_combo[0]]=0
        train_labels[train_labels==pid_combo[1]]=1

        #pca
        print("training pca...")
        pca = PCA()
        train_data = pca.fit(train_data).transform(train_data)

        print("training classification rf...")
        classification = get_trained_classifier_or_regressor(train_data,train_labels,'classification',cfg)
        classifier_s.append( classification )
        pca_s.append(pca)
        
        # now apply the classifiers to the mc data
        classification_all_np_array_buf = apply_pandas_to_sklearn_mapping( df,sklearn_pd_mapper )
        all_data = classification_all_np_array_buf[:,1:]
        all_data = pca.transform(all_data)

        index_buffer = pd.MultiIndex.from_tuples( df.index )
        predictions = classification.predict_proba( all_data )

        df_buf = pd.DataFrame({('ARA_pid_%d_to_%d_ness' % pid_combo) : pd.Series(predictions[:,0], index=index_buffer)})
        df.loc[(df_buf.index),('ARA_pid_%d_to_%d_ness' % pid_combo)] = df_buf[('ARA_pid_%d_to_%d_ness' % pid_combo)]
    
    return pca_s,classifier_s,sklearn_pd_mappers,pid_combinations

def get_balance_mask_data( df_mc, df_data, pid_combo ):
    n_mc = sum(df_mc['particle id (particle code or A x 100 + Z for nuclei)']==pid_combo[0])
    print("%d entries in " % n_mc,end="")
    print("pid %d classif. set" % pid_combo[0])
    n_data = sum(df_data['particle id (particle code or A x 100 + Z for nuclei)']==pid_combo[1])
    print("%d entries in " % n_data,end="")
    print("pid %d classif. set" % pid_combo[1])

    #save which one is smaller and if both have an equal number, return true
    #smaller = 1
    if n_mc >= n_data:
        raise ValueError('Not enough data for training, %f\% missing'%(1.0-n_data/n_mc))
    
    #downsample the data
    print("pid %d larger set than pid %d in classif. set, downsampling." % pid_combo)
    #save all entries with the smaller set
    mc_balance_mask = df_mc['particle id (particle code or A x 100 + Z for nuclei)']==pid_combo[0]

    #reduce the entries with the data set
    downsample = df_data.sample(n_mc)
    data_balance_mask = np.in1d(df_data.index.values,downsample.index.values)

    n_mc = sum(df_mc[mc_balance_mask]['particle id (particle code or A x 100 + Z for nuclei)']==pid_combo[0])
    print("downsampled to %d entries in " % n_mc,end="")
    print("pid %d classif. set" % pid_combo[0])
    n_data = sum(df_data[data_balance_mask]['particle id (particle code or A x 100 + Z for nuclei)']==pid_combo[1])
    print("downsampled to %d entries in " % n_data,end="")
    print("pid %d classif. set" % pid_combo[1])

    return mc_balance_mask,data_balance_mask

def train_and_apply_classifiers_on_data( df_mc, df_data, cfg, mc_pid=1, data_pid=14 ):
    #first get the mc "gammas" or any other pid specified
    mc_pid_mask = df_mc['particle id (particle code or A x 100 + Z for nuclei)']==mc_pid
    mc_train_mask = df_mc['training set'] 

    #assign a column of the data_pid to the data_df
    df_data['particle id (particle code or A x 100 + Z for nuclei)'] = data_pid

    # allocate variables for returning
    pid_combo = (mc_pid, data_pid)
    classifier_s = []
    pca_s = []
    sklearn_pd_mappers = []

    print("Building classifier: %d-to-%d-ness" % (pid_combo))
    # generate new columns in the dfs for the classifiers and default them nan
    df_mc['ARA_pid_%d_to_%d_ness' % pid_combo]=np.empty(len(df_mc),dtype="float32")
    df_mc['ARA_pid_%d_to_%d_ness' % pid_combo]=np.NAN
    df_data['ARA_pid_%d_to_%d_ness' % pid_combo]=np.empty(len(df_data),dtype="float32")
    df_data['ARA_pid_%d_to_%d_ness' % pid_combo]=np.NAN

    # get a mask such that the pids are equally represented ( e.g. 50k protons, 50k gammas )
    mc_balance_mask, data_balance_mask = get_balance_mask_data( df_mc[mc_pid_mask&mc_train_mask], df_data, pid_combo )
    mc_train_data_masks = ( mc_pid_mask&mc_train_mask&mc_balance_mask )
    data_train_data_masks = ( data_balance_mask )

    #merge the data and the mc frame in order to do the scaling and training
    df_training = pd.concat([df_mc[mc_train_data_masks], df_data[data_train_data_masks]], ignore_index=True)

    #fist get mapper
    sklearn_pd_mapper = get_pandas_to_sklearn_mapping( df_training,'classification',cfg )
    sklearn_pd_mappers.append(sklearn_pd_mapper)

    #then train
    classification_np_array_buf = apply_pandas_to_sklearn_mapping( df_training,sklearn_pd_mapper )
    #index_buffer = pd.MultiIndex.from_tuples( df_training.index )

    train_data = classification_np_array_buf[:,1:] # the data, comes after the target column
    train_labels = classification_np_array_buf[:,0].astype(int) # target has to be in the first column, pass it as string
    train_labels[train_labels==pid_combo[0]]=0
    train_labels[train_labels==pid_combo[1]]=1

    #pca
    print("training pca...")
    pca = PCA()
    train_data = pca.fit(train_data).transform(train_data)

    print("training classification rf...")
    classification = get_trained_classifier_or_regressor(train_data,train_labels,'classification',cfg)
    classifier_s.append( classification )
    pca_s.append(pca)
    
    # now apply the classifiers to the mc 
    classification_all_np_array_buf = apply_pandas_to_sklearn_mapping( df_mc,sklearn_pd_mapper )
    all_data = classification_all_np_array_buf[:,1:]
    all_data = pca.transform(all_data)

    index_buffer = pd.MultiIndex.from_tuples( df_mc.index )
    predictions = classification.predict_proba( all_data )

    df_buf = pd.DataFrame({('ARA_pid_%d_to_%d_ness' % pid_combo) : pd.Series(predictions[:,0], index=index_buffer)})
    df_mc.loc[(df_buf.index),('ARA_pid_%d_to_%d_ness' % pid_combo)] = df_buf[('ARA_pid_%d_to_%d_ness' % pid_combo)]

    return pca_s,classifier_s,sklearn_pd_mappers,[pid_combo]

def apply_classifiers_to_data( df, classifier_s, pca_s, mapper_s, pid_combos ):

    if len(mapper_s) is not len(pid_combos):
        print("unequal legths: mapper_s != pid_combos")
        return 
    if len(mapper_s) is not len(pca_s):
        print("unequal legths: mapper_s != pca_s")
        return 
    if len(mapper_s) is not len(classifier_s):
        print("unequal legths: mapper_s != classifier_s")
        return 

    df["particle id (particle code or A x 100 + Z for nuclei)"]=-1

    for i,pid_combo in enumerate(pid_combos):
        #print("Applying classifier: %d-to-%d-ness" % (pid_combo))

        # generate new columns in the df for the classifiers and default them nan
        df['ARA_pid_%d_to_%d_ness' % pid_combo]=np.empty(len(df),dtype="float32")
        df['ARA_pid_%d_to_%d_ness' % pid_combo]=np.NAN

        # now apply the classifiers to the mc data
        classification_all_np_array_buf = apply_pandas_to_sklearn_mapping( df,mapper_s[i] )
        all_data = classification_all_np_array_buf[:,1:]
        all_data = pca_s[i].transform(all_data)

        index_buffer = pd.MultiIndex.from_tuples( df.index )
        predictions = classifier_s[i].predict_proba( all_data )

        df_buf = pd.DataFrame({('ARA_pid_%d_to_%d_ness' % pid_combo) : pd.Series(predictions[:,0], index=index_buffer)})
        df.loc[(df_buf.index),('ARA_pid_%d_to_%d_ness' % pid_combo)] = df_buf[('ARA_pid_%d_to_%d_ness' % pid_combo)]
    
    df = df.drop('particle id (particle code or A x 100 + Z for nuclei)',1)

    return df
