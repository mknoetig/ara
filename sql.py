import sqlalchemy
import os
import pandas as pd

from fact_credentials import get_credentials

standard_query_pass0_2 = "SELECT fNight, fRunID, fRunStart, fRunStop FROM factdata.RunInfo WHERE fRunTypeKey=1 AND fSourceKey=5 AND fZenithDistanceMean<=35 AND fThresholdMedian <= 400 AND ((fNumExt1Trigger BETWEEN 10 AND 300) OR (fNumExt2Trigger BETWEEN 10 AND 300));"

def get_runinfo():
    if os.path.exists("runinfo.h5"):
        with pd.HDFStore("runinfo.h5") as store:
            runinfo = store["runinfo"]
    else:
        creds = get_credentials()
        spec = 'mysql+pymysql://{user}:{password}@129.194.168.95/{database}'
        factdb = sqlalchemy.create_engine(spec.format(**creds['database']))
        print("reading complete RunInfo table, takes about 1min.")
        runinfo = pd.read_sql_table("RunInfo", factdb)
        with pd.HDFStore("runinfo.h5") as store:
            store.put("runinfo", runinfo)
    return runinfo

def get_query(query):
    creds = get_credentials()
    spec = 'mysql+pymysql://{user}:{password}@129.194.168.95/{database}'
    factdb = sqlalchemy.create_engine(spec.format(**creds['database']))
    return factdb.execute(query)