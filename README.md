# Applied Reconstruction Algorithms (ARA) #

This program is written in order to complete the low-level analysis of FACT-telescope data from  [fact-tools output](https://github.com/fact-project/fact-tools) up to fully parametrized data, including telescope response functions. In this sense we hope to make the FACT analysis accessible to everyone. 

### Install ###

We strongly recommend installing **python3** via [Anaconda](https://www.continuum.io/downloads). Available for OSX and Linux. After having installed py3, checkout this repo

```
git clone https://mknoetig@bitbucket.org/mknoetig/ara.git

```

Then run pip on the requirements.txt, in order to get the dependencies
```
pip install -r requirements.txt
```

### secret knowledge ###


source code of MMCS

    phido:/sl6/sw/projects/fact/Mmcs

Where to find corsika files

    phido:/fhgfs/groups/app/fact/simulated/corsika
    isdc:/gpfs1/scratch/jbbuss/MonteCarloProduction/MonteCarlo/corsika

Ceres files here:

    isdc:/gpfs1/scratch/jbbuss/MonteCarloProduction/MonteCarlo/ceres