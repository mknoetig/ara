"""Apply models to one data file, outputs the fully classified data

Usage:
  apply_ara.py <models_file> <data_hdf5_in_file> <data_hdf5_out_file>
  apply_ara.py (-h | --help)
  apply_ara.py --version

Options:
  -h --help     Show this screen.
  --version     Show version.
"""

from __future__ import absolute_import, print_function, division

import os
import json
import sys
import pickle

import numpy as np
import pandas as pd

from tqdm import tqdm
from docopt import docopt

from hdf_storage import get_events_df_from_hdf5
from sql import get_runinfo
import inspecting
import training_and_test as T_T
from sources import *

# ----- the start of the main -----------
if __name__ == "__main__":

    #read models and initialize program
    arguments = docopt(__doc__, version='Ara 1')
    print('reading models file...')
    with open(arguments["<models_file>"], 'rb') as handle:
        models_file = pickle.load(handle)

    energy_models = models_file['energy_models']
    classifier_models = models_file['classifier_models']
    Aeff = models_file['Aeff']
    configuration = models_file['configuration']
    chunksize = 100000

    print("reading data from hdf5 files...")
    in_store = pd.HDFStore(arguments["<data_hdf5_in_file>"])
    nrows = in_store.get_storer("evts").nrows
    event_iterator = in_store.select(
        "evts", 
        chunksize = chunksize,
    )

    print("open the output file")
    out_store = pd.HDFStore(arguments["<data_hdf5_out_file>"])
    
    print("loading run data base")
    #get the data from the rundb and for later joining it to the data_evts
    runinfo = get_runinfo()
    runinfo = inspecting.drop_db_columns( runinfo,configuration ) 
    runinfo.rename(columns={'fNight': 'NIGHT', 'fRunID': 'RUNID'}, inplace=True)
    runinfo = inspecting.recast_columns_to_32(runinfo)
    runinfo = runinfo.set_index(['NIGHT','RUNID'])   

    #loop over all the events in the data file
    for events_df in tqdm(event_iterator,total=np.ceil(nrows/chunksize),desc="looping over chunks in file: "+arguments["<data_hdf5_in_file>"]): 
        events_df = events_df.set_index(['NIGHT','RUNID','EventNum']) 
        inspecting.prepare_df( events_df )
        inspecting.drop_nan_and_inf_inplace( events_df )

        # apply the regressors and classifiers to the data
        events_df = T_T.apply_energy_regressors_to_data(
            events_df,
            energy_models['energy_regressors'],
            energy_models['sklearn_energy_mappers'],
            energy_models['pid_set'])
        events_df = inspecting.recast_columns_to_32( events_df ) #recast the 64bit columns to save space 
        events_df = T_T.apply_classifiers_to_data(
            events_df,
            classifier_models['classifiers'],
            classifier_models['pcas'],
            classifier_models['sklearn_class_mappers'],
            classifier_models['pid_combos'])
        events_df = inspecting.recast_columns_to_32( events_df ) #recast the 64bit columns to save space 

        #join on the common index
        events_df=events_df.reset_index('EventNum')
        events_df=events_df.join(runinfo, how='inner')
        events_df=events_df.set_index('EventNum', append=True).reorder_levels(['NIGHT', 'RUNID', 'EventNum']).sortlevel()
        events_df.columns = [c.replace(' ', '_') for c in events_df.columns]
        events_df.columns = [c.replace('-', '_') for c in events_df.columns]

        #save the data to the new store
        try:
            nrows = out_store.get_storer('evts').nrows
        except:
            nrows = 0

        out_store.append('evts',events_df,data_columns=[
            'fSourceKEY',
            'fZenithDistanceMean',
            'ARA_pid_1_to_14_ness',
            'ARA_pid_1_mean_Energy_Prediction_in_GeV',
            'Theta',
            'Theta_Off_1',
            'Theta_Off_2',
            'Theta_Off_3',
            'Theta_Off_4',
            'Theta_Off_5'])

    in_store.close()
    out_store.close()

