import numpy as np
import pandas as pd

import corner
import emcee

import matplotlib.pyplot as plt
import statistics as st
from sources import *

from sklearn.metrics import roc_curve, auc
from scipy.optimize import fminbound
from scipy.interpolate import interpolate
from scipy.optimize import minimize
from scipy import integrate

FACT_deg_per_mm = 0.01172
sed_data_relative_path = "data_for_comparison/crab_pulsar_literature/extracted/sed/"
sed_fermi_data_files = [
    "fermi_lat_P1_upper.dat",
    "fermi_lat_P1_middle.dat",
    "fermi_lat_P1_lower.dat",
    "fermi_lat_P2_upper.dat",
    "fermi_lat_P2_middle.dat",
    "fermi_lat_P2_lower.dat"
]
sed_magic_data_files = [
    "magic_P1_upper.dat",
    "magic_P1_middle.dat",
    "magic_P1_lower.dat",
    "magic_P2_upper.dat",
    "magic_P2_middle.dat",
    "magic_P2_lower.dat"
]
sed_veritas_data_files = [
    "veritas_upper_flux_points.dat",
    "veritas_middle_flux_points.dat",
    "veritas_lower_flux_points.dat"
]
lc_data_relative_path = "data_for_comparison/crab_pulsar_literature/extracted/lc/"
lc_files = [
    "magic_50_400_GeV.dat",
    "fermi_lat_100_300_MeV.dat"
]
nebula_data_relative_path = "data_for_comparison/crab_pulsar_literature/extracted/nebula/"
nebula_files = [
    "crab_nebula_fermi_magic_combined.dat"
]

erg_per_TeV = 0.62415091

def effective_area(events_df,pid, cut_name,cut_map):

    A_thrown = 0.0
    if pid == 1:
        A_thrown = 270.0*270.0*np.pi # fabian gammas 
    else:
        A_thrown = 400.0*400.0*np.pi # fabian protons 

    E_min_log = np.log10(200)
    E_max_log = np.log10(50000)
    n_bins = 30

    bins = np.logspace(E_min_log,E_max_log,n_bins)

    pid_mask = (events_df['particle id (particle code or A x 100 + Z for nuclei)'] == pid)
    
    facttools_mask = events_df["survived_cut"] >= cut_map[cut_name]

    hthrown = np.histogram(events_df[pid_mask]['total energy in GeV'].values, bins)
    hsurvived = np.histogram(events_df[facttools_mask&pid_mask]['total energy in GeV'].values, bins)

    rate_calc = np.array([ st.binomial_credible_interval(int(hthrown[0][i]),int(hsurvived[0][i])) for i in range(len(hthrown[0])) ])
    rate_calc = rate_calc * A_thrown

    return rate_calc[:,1], hthrown[1], rate_calc[:,0],rate_calc[:,2],pid

def plot_effective_area(A_eff,label):

    x=A_eff[1][1:]
    plt.step(x, A_eff[0], 'k', label=label )
    plt.step(x, A_eff[2], 'k', alpha=0.35)
    plt.step(x, A_eff[3], 'k', alpha=0.35)
    plt.loglog()

    plt.xlabel("E$_{true}$ / GeV")
    plt.ylabel("A$_{eff}$ / m$^2$")
    plt.title("A$_{eff}$ for %s" % stringify(A_eff[4]) )
    plt.legend(loc='best')

def plot_Migration_matrix(pid,events_df):
    pid_mask = (events_df['particle id (particle code or A x 100 + Z for nuclei)'] == pid)
    test_mask = (events_df['testing set'])

    hist_pars = plt.hist2d(
        np.log10(events_df[ pid_mask&test_mask ]['total energy in GeV']),
        np.log10(events_df[ pid_mask&test_mask ]['ARA_pid_%d_mean_Energy_Prediction_in_GeV' % pid]),
        bins=(40,40),cmap='viridis',cmin=1
        )

    plt.ylabel(("log$_{10}$(E$_{est}$ / GeV)"), fontsize=14)
    plt.xlabel("log$_{10}$(E$_{true}$ /GeV)", fontsize=14)
    plt.xticks(fontsize=14)
    plt.yticks(fontsize=14)

    return hist_pars

def plot_Migration_matrix_comparison_magic(events_df):
    hist_pars = plt.hist2d(
        np.log10(events_df['Magic_Energy']),
        np.log10(events_df['ARA_pid_1_mean_Energy_Prediction_in_GeV']),
        bins=(40,40),cmap='viridis',cmin=1
        )

    plt.ylabel(("log$_{10}$(E$_{est}$ / GeV)"), fontsize=14)
    plt.xlabel("log$_{10}$(E$_{true}$ /GeV)", fontsize=14)
    plt.xticks(fontsize=14)
    plt.yticks(fontsize=14)

    return hist_pars

def Profile(ax,x,y,nbins,xmin,xmax):
    df = pd.DataFrame({'x' : x , 'y' : y})

    binedges = xmin + ((xmax-xmin)/nbins) * np.arange(nbins+1)
    df['bin'] = np.digitize(df['x'],binedges)

    bincenters = xmin + ((xmax-xmin)/nbins)*np.arange(nbins) + ((xmax-xmin)/(2*nbins))
    ProfileFrame = pd.DataFrame({'bincenters' : bincenters, 'N' : df['bin'].value_counts(sort=False)},index=range(1,nbins+1))
    #ProfileFrame.replace(np.nan, 0,inplace=True)  

    bins = ProfileFrame.index.values
    for bin in bins:
        ProfileFrame.ix[bin,'ymean'] = df.ix[df['bin']==bin,'y'].mean()
        ProfileFrame.ix[bin,'yStandDev'] = df.ix[df['bin']==bin,'y'].std()
        ProfileFrame.ix[bin,'yMeanError'] = ProfileFrame.ix[bin,'yStandDev'] / np.sqrt(ProfileFrame.ix[bin,'N'])

    #ProfileFrame.replace(np.nan, 0,inplace=True) 
    ProfileFrame.dropna(inplace=True)

    ax.errorbar(
        ProfileFrame['bincenters'].values, 
        ProfileFrame['ymean'].values, 
        yerr=ProfileFrame['yMeanError'].values, 
        xerr=(xmax-xmin)/(2*nbins), fmt='+',
        color="black",label='norm. profile') 
    return ax

def ProfileStdev(ax,x,y,nbins,xmin,xmax):
    df = pd.DataFrame({'x' : x , 'y' : y})

    binedges = xmin + ((xmax-xmin)/nbins) * np.arange(nbins+1)
    df['bin'] = np.digitize(df['x'],binedges)

    bincenters = xmin + ((xmax-xmin)/nbins)*np.arange(nbins) + ((xmax-xmin)/(2*nbins))
    ProfileFrame = pd.DataFrame({'bincenters' : bincenters, 'N' : df['bin'].value_counts(sort=False)},index=range(1,nbins+1))
    #ProfileFrame.replace(np.nan, 0,inplace=True)  

    bins = ProfileFrame.index.values
    for bin in bins:
        buf = 1.0
        if ProfileFrame.ix[bin,'N'] > 1.0:
            buf =  np.sqrt(2.*(ProfileFrame.ix[bin,'N']-1))
        ProfileFrame.ix[bin,'yStandDev'] = df.ix[df['bin']==bin,'y'].std()
        ProfileFrame.ix[bin,'yMeanError'] = ProfileFrame.ix[bin,'yStandDev'] / buf

    #ProfileFrame.replace(np.nan, 0,inplace=True) 
    ProfileFrame.dropna(inplace=True)

    ax.errorbar(
        ProfileFrame['bincenters'].values, 
        ProfileFrame['yStandDev'].values/(10**ProfileFrame['bincenters'].values),
        yerr=ProfileFrame['yMeanError'].values/(10**ProfileFrame['bincenters'].values), 
        xerr=(xmax-xmin)/(2*nbins), fmt='o',
        color="black",label='stdev') 
    return ax

def plot_energy_resolution(pid,axis,events_df,histogram_pars):
    pid_mask = (events_df['particle id (particle code or A x 100 + Z for nuclei)'] == pid)
    test_mask = (events_df['testing set'])

    trueE = events_df[ pid_mask&test_mask ]['total energy in GeV']
    predE = events_df[ pid_mask&test_mask ]['ARA_pid_%d_mean_Energy_Prediction_in_GeV' % pid]
    # make the yvalues relative
    relEError = ( predE - trueE )/trueE

    Profile(axis,(np.log10(trueE)).values,relEError.values,len(histogram_pars[1])-1,histogram_pars[1].min(),histogram_pars[1].max())

    plt.legend()
    plt.xlabel("log$_{10}$(E$_{true}$/GeV)", fontsize=14)
    plt.xticks(fontsize=14)
    plt.yticks(fontsize=14)

def plot_energy_resolution_comparison_magic(axis,events_df,histogram_pars):
    trueE = events_df['Magic_Energy']
    predE = events_df['ARA_pid_1_mean_Energy_Prediction_in_GeV']
    # make the yvalues relative
    relEError = ( predE - trueE )/trueE

    Profile(axis,(np.log10(trueE)).values,relEError.values,len(histogram_pars[1])-1,histogram_pars[1].min(),histogram_pars[1].max())

    plt.legend()
    plt.xlabel("log$_{10}$(Magic Energy/GeV)", fontsize=14)
    plt.xticks(fontsize=14)
    plt.yticks(fontsize=14)

def plot_energy_error(pid,axis,events_df,histogram_pars):
    pid_mask = (events_df['particle id (particle code or A x 100 + Z for nuclei)'] == pid)
    test_mask = (events_df['testing set'])

    trueE = events_df[ pid_mask&test_mask ]['total energy in GeV']
    predE = events_df[ pid_mask&test_mask ]['ARA_pid_%d_mean_Energy_Prediction_in_GeV' % pid]
    # make the yvalues relative
    EError = ( predE - trueE )

    ProfileStdev(axis,(np.log10(trueE)).values,EError.values,len(histogram_pars[1])-1,histogram_pars[1].min(),histogram_pars[1].max())

    plt.legend()
    plt.xticks(fontsize=14)
    plt.yticks(fontsize=14)

def plot_energy_error_comparison_magic(axis,events_df,histogram_pars):
    trueE = events_df['Magic_Energy']
    predE = events_df['ARA_pid_1_mean_Energy_Prediction_in_GeV']
    # make the yvalues relative
    EError = ( predE - trueE )

    ProfileStdev(axis,(np.log10(trueE)).values,EError.values,len(histogram_pars[1])-1,histogram_pars[1].min(),histogram_pars[1].max())

    plt.legend()
    plt.xticks(fontsize=14)
    plt.yticks(fontsize=14)

def stringify(pid):
    if pid == 1:
        return "gamma"
    if pid == 14:
        return "proton"
    return str(pid)

def plot_pid_to_pid_ness_histo( events_df, pid0, pid1 ):

    left_pid_mask  = (events_df['particle id (particle code or A x 100 + Z for nuclei)'] == pid0)
    right_pid_mask = (events_df['particle id (particle code or A x 100 + Z for nuclei)'] == pid1)
    test_mask = (events_df['testing set'])

    left_array = events_df[ left_pid_mask&test_mask ]['ARA_pid_%d_to_%d_ness' % (pid0,pid1)]
    right_array= events_df[ right_pid_mask&test_mask ]['ARA_pid_%d_to_%d_ness' % (pid0,pid1)]

    nbins=50
    bins=np.linspace(0,1,nbins+1)

    plt.hist(left_array,normed=True,bins=bins,histtype='step',ls='solid',color='black',label="%s" % stringify(pid0))
    plt.hist(right_array,normed=True,bins=bins,histtype='step',ls='--',color='black',label="%s" % stringify(pid1))

    plt.xlabel("confidence", fontsize=12)
    plt.title("P(%s-to-%s confidence)" % (stringify(pid0),stringify(pid1)))
    plt.xticks(fontsize=12)
    plt.yticks(fontsize=12)
    plt.legend()

def plot_pid_to_pid_ness_survival( events_df, pid0, pid1 ):
    left_pid_mask  = (events_df['particle id (particle code or A x 100 + Z for nuclei)'] == pid0)
    right_pid_mask = (events_df['particle id (particle code or A x 100 + Z for nuclei)'] == pid1)
    test_mask = (events_df['testing set'])

    left_array = events_df[ left_pid_mask&test_mask ]['ARA_pid_%d_to_%d_ness' % (pid0,pid1)]
    right_array= events_df[ right_pid_mask&test_mask ]['ARA_pid_%d_to_%d_ness' % (pid0,pid1)]

    nbins=50
    bins=np.linspace(0,1,nbins+1)

    plt.hist(left_array,normed=True,bins=bins,histtype='step',ls='solid',color='black',label="%s" % stringify(pid0),cumulative=-1)
    plt.hist(right_array,normed=True,bins=bins,histtype='step',ls='--',color='black',label="%s" % stringify(pid1),cumulative=-1)

    plt.xlabel("confidence", fontsize=12)
    plt.title("Survival(%s-to-%s confidence)" % (stringify(pid0),stringify(pid1)))
    plt.xticks(fontsize=12)
    plt.yticks(fontsize=12)
    plt.xlim(0,1)
    plt.ylim(0,1)
    plt.legend()

def plot_roc_curve( events_df, pid0, pid1 ):
    test_mask = (events_df['testing set'])

    labels = events_df[test_mask]['particle id (particle code or A x 100 + Z for nuclei)'].copy()
    
    labels.loc[labels==pid0] = 1 
    labels.loc[labels==pid1] = 0

    data = events_df[test_mask]['ARA_pid_%d_to_%d_ness' % (pid0,pid1)]

    fpr, tpr, _ = roc_curve(labels, data)
    roc_auc = auc(fpr, tpr)

    plt.plot(fpr, tpr, label='area under curve = %0.3f)' % roc_auc,color='black')
    plt.plot([0, 1], [0, 1], 'k--',label='chance')
    plt.xlim([0.0, 1.])
    plt.ylim([0.0, 1.])
    plt.xlabel('False Positive Rate (%s survival)'%stringify(pid1))
    plt.ylabel('True Positive Rate (%s survival)'%stringify(pid0))
    plt.title('ARA %s-to-%s confidence ROC curve' % (stringify(pid0),stringify(pid1)))
    plt.legend(loc='best')


def plot_theta_2_histo( events_df, pid, annotation="", normed=True ):
    pid_mask  = (events_df['particle id (particle code or A x 100 + Z for nuclei)'] == pid)
    test_mask = (events_df['testing set'])

    events_array = events_df[ pid_mask&test_mask ]['Theta']*events_df[ pid_mask&test_mask ]['Theta']*FACT_deg_per_mm*FACT_deg_per_mm
    
    nbins=20
    bins=np.linspace(0,0.2,nbins+1)

    ret = plt.hist(events_array,normed=normed,bins=bins,histtype='step',label="pid %d %s" % (pid,annotation))
    
    plt.xlabel("$\\vartheta^2$", fontsize=12)
    plt.xticks(fontsize=12)
    plt.yticks(fontsize=12)
    plt.legend()
    return ret

def plot_theta_2_cdf( events_df, pid, annotation="", normed=True ):
    pid_mask  = (events_df['particle id (particle code or A x 100 + Z for nuclei)'] == pid)
    test_mask = (events_df['testing set'])

    events_array = events_df[ pid_mask&test_mask ]['Theta']*events_df[ pid_mask&test_mask ]['Theta']*FACT_deg_per_mm*FACT_deg_per_mm

    stepsize = 0.01
    bins=np.arange(0,events_array.max()+stepsize,stepsize)

    plt.hist(events_array,normed=normed,bins=bins,histtype='step',label="pid %d %s" % (pid,annotation),cumulative=1)
    
    plt.title("CDF($\\vartheta^2$)", fontsize=12)
    plt.xlabel("$\\vartheta^2$", fontsize=12)
    plt.xticks(fontsize=12)
    plt.yticks(fontsize=12)
    plt.ylim(0,1)
    plt.xlim(0,0.2)
    plt.legend(loc='best')

def plot_theta_2_roc( events_df, pid0, pid1, print_chance=True, annotation='' ):
    test_mask = (events_df['testing set'])

    labels = events_df[test_mask]['particle id (particle code or A x 100 + Z for nuclei)'].copy()
    
    labels.loc[labels==pid0] = 1 
    labels.loc[labels==pid1] = 0

    data = events_df[test_mask]['Theta']
    data = 1.0 - data/data.max()

    fpr, tpr, _ = roc_curve(labels, data)
    roc_auc = auc(fpr, tpr)

    plt.plot(fpr, tpr, label='ROC area (auc) = %0.3f %s' % (roc_auc, annotation))
    if print_chance: 
        plt.plot([0, 1], [0, 1], 'k--',label='chance')
    plt.xlim([0.0, 1.])
    plt.ylim([0.0, 1.])
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.title('$\\vartheta$-cut pid %d-to-%d ROC-curve' % (pid0,pid1))
    plt.legend(loc='best')

def plot_disp0_dispRF_error( events_df, pid, annotation='' ):
    pid_mask  = (events_df['particle id (particle code or A x 100 + Z for nuclei)'] == pid)
    test_mask = (events_df['testing set'])

    disp=events_df[pid_mask&test_mask]['Disp'].copy()
    dist=events_df[pid_mask&test_mask]['Distance'].copy()
    dispRF=events_df[pid_mask&test_mask]['ARA_disp_regressor'].copy()

    disp=disp*FACT_deg_per_mm
    dist=dist*FACT_deg_per_mm
    dispRF=dispRF*FACT_deg_per_mm

    disp_err = disp-dist
    dispRF_err = dispRF-dist

    min_bin=min(disp_err.min(),dispRF_err.min())
    max_bin=max(disp_err.max(),dispRF_err.max())

    nbins=40
    bins=np.linspace(min_bin,max_bin,nbins+1)

    std_disp_err = np.std(disp_err)
    std_dispRF_err = np.std(dispRF_err)

    plt.hist(disp_err,bins=bins, histtype='step', label=('disp-dist\n$\sigma$=%0.3f$^{\circ}$ %s' % (std_disp_err,annotation)))
    plt.hist(dispRF_err,bins=bins, histtype='step', label=('dispRF-dist\n$\sigma$=%0.3f$^{\circ}$ %s' % (std_dispRF_err,annotation)))

    plt.xlabel("residual in $^{\circ}$", fontsize=12)
    plt.title("Disp error pid %d" % pid)
    plt.xticks(fontsize=12)
    plt.yticks(fontsize=12)
    plt.legend(loc='best')

def plot_data_mc_comparison( mc_df, data_df, key, log10=False, logy=False, loc='best' ):
    pid_set = set(mc_df['particle id (particle code or A x 100 + Z for nuclei)'])
    pid_masks = [ mc_df['particle id (particle code or A x 100 + Z for nuclei)'] == i  for i in pid_set ]
    
    mc_events_selected = [ mc_df[mask][key].copy() for mask in pid_masks ]
    data_events_selected = data_df[key].copy()

    datalabel="data"
    mclabel="mc"
    xlabel=""
    if log10:
        xlabel=("log$_{10}$(%s)" % key)
    else:
        xlabel=("%s" % key)
    
    if log10:
        mc_events_selected = [ np.log10(i) for i in mc_events_selected ]
        data_events_selected = np.log10(data_events_selected)

    n,bins,patches = plt.hist( data_events_selected.values,histtype='step', bins=40, label=datalabel, normed=True, color='black')
    [ plt.hist( mc_events_selected[i].values,histtype='step', bins=bins, label=mclabel+" "+stringify(pid), normed=True ) for i,pid in enumerate(pid_set) ]
    
    if logy: 
        plt.semilogy()
    plt.xlabel(xlabel)
    plt.ylim([np.min(n)*0.5,np.max(n)*1.5])
    plt.legend(loc=loc)
    
    plt.xticks(fontsize=12)
    plt.yticks(fontsize=12)

    return n,bins,patches

def plot_magic_comparison( data_df, key, log10=False, logy=False, loc='best', xlim="" ):
    data_events_selected = data_df[key].copy()

    datalabel="magic selected"
    xlabel=""
    if log10:
        xlabel=("log$_{10}$(%s)" % key)
    else:
        xlabel=("%s" % key)
    
    if log10:
        data_events_selected = np.log10(data_events_selected)

    n,bins,patches = plt.hist( data_events_selected.values,histtype='step', bins=40, label=datalabel, normed=True )
    
    if logy: 
        plt.semilogy()
    plt.xlabel(xlabel)
    plt.ylim([np.min(n)*0.5,np.max(n)*1.5])
    if xlim != "":
        plt.xlim(xlim)
    plt.legend(loc=loc)
    plt.xticks(fontsize=12)
    plt.yticks(fontsize=12)

    return n,bins,patches

def theta_2_histo_data( events_df, gamma_cut, theta2_cut, source='all', plotting=True, energy_cut=[ 0, 1000000000] ):
    gamma_mask  = (events_df['ARA_pid_1_to_14_ness'] >= gamma_cut)
    energy_mask = (events_df["ARA_pid_1_mean_Energy_Prediction_in_GeV"] > energy_cut[0])
    energy_mask = energy_mask&(events_df["ARA_pid_1_mean_Energy_Prediction_in_GeV"] <= energy_cut[1])
    source_mask = gamma_mask.copy()
    if source is not 'all':
        source_mask = (events_df.fSourceKEY == source_map[source])
    else:
        source_mask = True
    all_mask = gamma_mask&source_mask&energy_mask
    
    on_time = events_df[source_mask].reset_index().groupby(['NIGHT','RUNID']).mean()['fOnTime'].sum()
    
    data_beg = str(events_df[source_mask].reset_index().NIGHT.min())
    data_end = str(events_df[source_mask].reset_index().NIGHT.max())

    on_evts = events_df[all_mask]['Theta'].values*events_df[all_mask]['Theta'].values*FACT_deg_per_mm*FACT_deg_per_mm
    off_evts= np.hstack([ events_df[all_mask]['Theta_Off_%d'%(i+1)].values*events_df[all_mask]['Theta_Off_%d'%(i+1)].values*FACT_deg_per_mm*FACT_deg_per_mm for i in range(5) ])
    
    N_on = np.sum(on_evts<=theta2_cut)
    N_off = np.sum(off_evts<=theta2_cut)
    alpha = 0.2
    N_ex = float(N_on)-float(N_off)*alpha
    sig = st.li_ma_significance(N_on,N_off,alpha)[0]

    nbins=60
    bins=np.linspace(0,0.3,nbins+1)

    if plotting: 
        plt.hist(on_evts,bins=bins,histtype='step',label="on",color='black')
        plt.hist(off_evts,weights=(np.zeros(len(off_evts))+0.2),bins=bins,histtype='step',label="off", color="black", ls = "--")
    
        ymin, ymax = plt.gca().get_ylim()
        plt.plot((theta2_cut, theta2_cut), (ymin, ymax), 'k-')
        plt.xlim([np.min(bins),np.max(bins)])
        
        plt.xlabel("$\\vartheta^2$", fontsize=12)
        plt.title("%s, %.02fh on time\n[%s --- %s]"%(source,on_time/60./60.,data_beg,data_end))
        plt.xticks(fontsize=12)
        plt.yticks(fontsize=12)
        plt.legend(loc='upper right')

    return N_on, N_off, N_ex, sig, on_time, data_beg, data_end

def read_from_file(filepath):
    return np.loadtxt(filepath)

def plot_lc_from_data(filename, axis):
    data = read_from_file(filename)
    for i in range(len(data)):
        if data[i][0] >= 0.5:
            data[i][0] = data[i][0]-1

    data = data[data[:,0].argsort()]
    label = "Magic (50-400)GeV\nA&A 565 (2014)"
    if "fermi" in filename[-40:]:
        label = "Fermi-LAT (100-300)MeV\nA&A 565 (2014)"
    axis.plot(data[:,0],data[:,1], 'k-', drawstyle='steps', label=label)
    axis.get_yaxis().set_visible(False)
    leg=axis.legend(loc='upper left', prop={'size':12})
    leg.get_frame().set_linewidth(0.0) 


def phasogramm_data( events_df, gamma_cut, theta2_cut, source='Crab', plotting=True, energy_cut=[ 0, 100000] ):
    
    #crab on and off regions in phase, from MAGIC A&A 585 (2016) 
    #+- 0.003 in phase becaue of 90 mus BAT residual error
    p0 = [-0.020,0.029]
    p1 = [0.374,0.425]
    ton= [p0,p1]

    gamma_mask  = (events_df['ARA_pid_1_to_14_ness'] >= gamma_cut)
    energy_mask = (events_df["ARA_pid_1_mean_Energy_Prediction_in_GeV"] > energy_cut[0])
    energy_mask = energy_mask&(events_df["ARA_pid_1_mean_Energy_Prediction_in_GeV"] <= energy_cut[1])
    source_mask = gamma_mask.copy()
    theta_mask  = (events_df['Theta'].values*events_df['Theta'].values*FACT_deg_per_mm*FACT_deg_per_mm <= theta2_cut)
    
    if source is not 'all':
        source_mask = (events_df.fSourceKEY == source_map[source])
    else:
        source_mask = True
    all_mask = gamma_mask&source_mask&energy_mask&theta_mask
    
    on_time = events_df[source_mask].reset_index().groupby(['NIGHT','RUNID']).mean()['fOnTime'].sum()
    
    data_beg = str(events_df[source_mask].reset_index().NIGHT.min())
    data_end = str(events_df[source_mask].reset_index().NIGHT.max())

    #get the phase events
    phase_evts = events_df[all_mask]['CrabPhase'].copy()
    phase_evts[phase_evts>=0.5]=phase_evts[phase_evts>=0.5]-1.0
    
    p0_mask = ( phase_evts <= p0[1] )&( phase_evts >= p0[0] )
    p1_mask = ( phase_evts <= p1[1] )&( phase_evts >= p1[0] )
    
    on_evts = phase_evts[ p0_mask|p1_mask ]
    off_evts = phase_evts[ ~(p0_mask|p1_mask) ]

    N_on = len(on_evts)
    N_off = len(off_evts)
    ton = p0[1]-p0[0] + p1[1]-p1[0]
    toff = 1.-ton

    alpha = ton/toff
    N_ex = float(N_on)-float(N_off)*alpha
    sig = st.li_ma_significance(N_on,N_off,alpha)[0]

    phase_binning = 0.025
    nbins = int(1./phase_binning)
    bins=np.linspace(-0.5,0.5,nbins+1)

    bg_rate_per_bin = N_off / (1/(alpha+1)) / nbins

    if plotting:

        f, (ax1, ax2, ax3) = plt.subplots(3, sharex=True,  gridspec_kw = {'height_ratios':[4, 1, 1]})

        f.subplots_adjust(hspace=0)
        ax1.hist(phase_evts,bins=bins,histtype='step',label="",color='black')
        
        plot_lc_from_data(lc_data_relative_path+lc_files[0], ax2)
        plot_lc_from_data(lc_data_relative_path+lc_files[1], ax3)
        
        ymin, ymax = plt.gca().get_ylim()
        
        ax1.axvspan(p0[0], p0[1], alpha=0.1, color='black')
        ax1.axvspan(p1[0], p1[1], alpha=0.1, color='black')
        ax2.axvspan(p0[0], p0[1], alpha=0.1, color='black')
        ax2.axvspan(p1[0], p1[1], alpha=0.1, color='black')
        ax3.axvspan(p0[0], p0[1], alpha=0.1, color='black')
        ax3.axvspan(p1[0], p1[1], alpha=0.1, color='black')
        
        ax1.set_xlim([np.min(bins),np.max(bins)])
        ax1.plot((np.min(bins), np.max(bins)), (bg_rate_per_bin, bg_rate_per_bin), 'k-', alpha=0.5, label="avg. off phase rate")
        
        ax3.set_xlabel("Crab Pulsar phase", fontsize=12)
        ax1.set_title("Counts/bin, %.02fh on time\n[%s --- %s]"%(on_time/60./60.,data_beg,data_end))
        leg=ax1.legend(loc='upper left', prop={'size':12})
        leg.get_frame().set_linewidth(0.0)

        # Fine-tune figure; make subplots close to each other and hide x ticks for
        # all but bottom plot.
        plt.setp([a.get_xticklabels() for a in f.axes[:-1]], visible=False)
        
    return N_on, N_off, alpha, N_ex, sig, on_time

def optimize_gamma_cut_on_theta_2_histo_data( events_df, theta2_cut,source='Crab',energy_cut=[ 0, 1000000000] ):

    lmin = 1e-11 
    lmax = 1-1e-11 
    
    #print lmin, lstar, lmax
    gamma_cut = fminbound(lambda x: (theta_2_histo_data(
        events_df, 
        gamma_cut=x, 
        theta2_cut=theta2_cut,
        source=source,
        plotting=False,
        energy_cut=energy_cut
        ))[3]*-1.0, lmin, lmax)
        
    return gamma_cut

def optimize_theta2_cut_on_theta_2_histo_data( events_df, gamma_cut,source='Crab',energy_cut=[ 0, 1000000000]  ):

    lmin = 0.005
    lmax = 0.05 
    
    #print lmin, lstar, lmax
    gamma_cut = fminbound(lambda x: (theta_2_histo_data(
        events_df, 
        gamma_cut=gamma_cut, 
        theta2_cut=x,
        source=source,
        plotting=False,
        energy_cut=energy_cut
        ))[3]*-1.0, lmin, lmax)  

    return gamma_cut

def beta_percent_of_bin( bins, beta=0.2 ):
    return bins[:-1]+beta*(bins[1:]-bins[:-1])

def get_rate_estimate( Non, Noff, alpha=0.2 ):
    
    Non = np.array(Non)
    Noff = np.array(Noff)
    Nex = Non - alpha*Noff
    Sqrt_Var = np.sqrt( Non + alpha*alpha*Noff )
    return Nex, Nex-Sqrt_Var, Nex+Sqrt_Var 

def get_a_eff_interpol_functions( A_eff  ):

    x=beta_percent_of_bin( np.log10(A_eff[1]),0.5 )
    y0=A_eff[0]
    y1=A_eff[2]
    y2=A_eff[3]

    ymode = interpolate.interp1d(x,y0,bounds_error=False,fill_value=0.)    
    ylow  = interpolate.interp1d(x,y1,bounds_error=False,fill_value=0.)    
    yhigh = interpolate.interp1d(x,y2,bounds_error=False,fill_value=0.)

    return ymode,ylow,yhigh

def fix_aeff_version_problem(Aeff_with_problem):

    x0=Aeff_with_problem[0].x
    x1=Aeff_with_problem[1].x
    x2=Aeff_with_problem[2].x    
    y0=Aeff_with_problem[0].y
    y1=Aeff_with_problem[1].y
    y2=Aeff_with_problem[2].y
    
    ymode = interpolate.interp1d(x0,y0,bounds_error=False,fill_value=0.)    
    ylow  = interpolate.interp1d(x1,y1,bounds_error=False,fill_value=0.)    
    yhigh = interpolate.interp1d(x2,y2,bounds_error=False,fill_value=0.)

    return ymode,ylow,yhigh

def add_one_bin( bins ):

    nbins = len(bins)-1
    new_bins = []

    for i in range(nbins+2):
        new_bins.append( bins[0] + float(i)*(bins[-1] - bins[0])/(nbins+1) )

    return new_bins

def double_bins( bins ):

    nbins = len(bins)-1
    new_bins = []

    for i in range(nbins*2+1):
        new_bins.append( bins[0] + float(i)*(bins[-1] - bins[0])/(nbins*2) )

    return new_bins

def increase_bins( bins ):

    nbins = len(bins)-1
    if nbins <= 4:
        return double_bins( bins )
    return add_one_bin( bins ) 

def guess_bins( bins, sigma, nmax=15 ):

    nbins = int(sigma*sigma/3./3.)
    if nbins > nmax: 
        nbins = nmax
    new_bins = []

    for i in range(nbins+1):
        new_bins.append( bins[0] + float(i)*(bins[-1] - bins[0])/(nbins) )
    return new_bins

def get_intervals_from_bins( bins ):
    list_of_intervals = []
    for i in range(len(bins)-1):
        list_of_intervals.append( [bins[i],bins[i+1]] )
    return list_of_intervals

def get_intelligent_countlist(data_evts,source,gamma_cut,theta2_cut,energy_range=[600,10000],nmax=15):

    logarithmic_bin_edges_old = np.log10(energy_range)

    #check significance
    N_on,N_off,N_ex,sig,on_time,beg,end=theta_2_histo_data(data_evts,source=source,gamma_cut=gamma_cut,theta2_cut=theta2_cut,plotting=False,energy_cut=(10**logarithmic_bin_edges_old))

    n_3_sigma_bins_before_split = np.sum(np.array([sig])>3)

    #if one bin has already less than 3 sigma, return what you got
    if n_3_sigma_bins_before_split == 0:
        return np.array([N_on]),np.array([N_off]),np.array([N_ex]),np.array([sig])

    N_on_list_old = []
    N_off_list_old = []
    N_ex_list_old = []
    sig_list_old = []

    logarithmic_bin_edges_new = guess_bins(logarithmic_bin_edges_old,sig,nmax=nmax)
    
    bin_intervals = get_intervals_from_bins( logarithmic_bin_edges_new )

    #calculate the individual bin counts
    for energy_interval in bin_intervals:
        N_on,N_off,N_ex,sig,buf1,buf2,buf3=theta_2_histo_data(data_evts,source=source,gamma_cut=gamma_cut,theta2_cut=theta2_cut,plotting=False,energy_cut=(10**np.array(energy_interval)))
        N_on_list_old.append(N_on)
        N_off_list_old.append(N_off)
        N_ex_list_old.append(N_ex)
        sig_list_old.append(sig)

    N_on_list_old = np.array(N_on_list_old)
    N_off_list_old = np.array(N_off_list_old)
    N_ex_list_old = np.array(N_ex_list_old)
    sig_list_old = np.array(sig_list_old)
    return N_on_list_old,N_off_list_old,N_ex_list_old,sig_list_old,logarithmic_bin_edges_new,on_time,beg,end


def plot_spectrum( data_evts, a_eff_interpol,source,gamma_cut,theta2_cut,energy_range=[700,15000],beta=0.2,energy_sdf_limit=1000.0,nmax=15):

    #get the count data.
    #[0] -> Non
    #[1] -> Noff
    #[2] -> Nex
    #[3] -> Sigma
    #[4] -> log10 energy bin edges
    #[5] -> on time
    #[6] -> beginning of observations
    #[7] -> end of observations
    count_data = get_intelligent_countlist(data_evts,source=source,gamma_cut=gamma_cut,theta2_cut=theta2_cut, energy_range=energy_range, nmax=nmax)
   
    energy_bin_edges = count_data[4]
    bin_intervals = np.array(get_intervals_from_bins( 10**np.array(energy_bin_edges) ))

    on_time = count_data[5]
    data_beg = count_data[6]
    data_end = count_data[7]

    deltaE = np.array(bin_intervals[:,1]-bin_intervals[:,0])/1000. # in TeV
    E_sampled = np.log10(np.array(beta_percent_of_bin( 10**np.array(energy_bin_edges),beta=beta )))
    E_logarithmic_mean = np.array(beta_percent_of_bin( np.array(energy_bin_edges),beta=0.5 ))

    #systematical uncertainty
    a_eff_low = np.array(a_eff_interpol[1]( E_sampled ))*10000. # in cm^2
    a_eff_mode = np.array(a_eff_interpol[0]( E_sampled ))*10000.
    a_eff_high = np.array(a_eff_interpol[2]( E_sampled ))*10000.

    #statistical uncertainty
    Nex_mean,Nex_low,Nex_high = get_rate_estimate(count_data[0],count_data[1])

    #calculate the flux, including statistical and systematical error
    dN_dE      = Nex_mean/deltaE/on_time/a_eff_mode
    dN_dE_low  = Nex_low/deltaE/on_time/a_eff_mode
    dN_dE_high = Nex_high/deltaE/on_time/a_eff_mode
    dN_dE_low_system  = Nex_low/deltaE/on_time/a_eff_high
    dN_dE_high_system = Nex_high/deltaE/on_time/a_eff_low

    dN_dE_errors = np.array([dN_dE - dN_dE_low, dN_dE_high - dN_dE])
    dN_dE_errors_system = np.array([dN_dE - dN_dE_low_system, dN_dE_high_system - dN_dE])

    bin_size = (np.power(10,np.array(energy_bin_edges[:-1]))-np.power(10,np.array(energy_bin_edges[1:])))/2.

    #only plot the bins with sigma > 3
    E_logarithmic_mean = E_logarithmic_mean[count_data[3]>=3.]
    dN_dE = dN_dE[count_data[3]>=3.]
    bin_size=bin_size[count_data[3]>=3.]
    dN_dE_errors_system=dN_dE_errors_system[:,count_data[3]>=3.]

    plt.errorbar(np.power(10,E_logarithmic_mean), dN_dE,xerr=bin_size,yerr=dN_dE_errors_system, fmt='.', label='FACT '+str(source), color="black")
    plt.loglog()

    plt.title("%s, %.02fh on time\n[%s --- %s]"%(source,on_time/60./60.,data_beg,data_end))
    plt.xticks(fontsize=12)
    plt.yticks(fontsize=12)
    plt.xlabel("E / GeV")
    plt.ylabel("dN/dE / [(cm$^2$ s TeV)$^{-1}$]")

    #now, interpolate the spectrum with power law, fit with symm. statistical error only
    sigma_yi = (dN_dE - dN_dE_low[count_data[3]>=3.])/dN_dE
    sigma_yi = np.log10( sigma_yi+1 )

    a, sig_a, b, sig_b, chi2, NDF = st.get_lin_fit(
        E_logarithmic_mean-3, 
        np.log10(dN_dE), 
        sigma_yi )

    alpha = b
    sig_alpha = sig_b
    f0 = 10**(a)
    sig_f0 = np.log(10)*sig_a*10**a
  
    #from this fit, calculate the flux > 1 TeV
    F_over_x_in_TeV = -f0*(energy_sdf_limit/1000.)**(1+alpha)/(1+alpha)
    buf1 = (sig_f0/f0)**2
    buf2 = ((np.log(energy_sdf_limit/1000.)*(1+alpha)-1)/(1+alpha)*sig_alpha)**2
    buf3 = np.sqrt( buf1 + buf2 )
    sig_F_over_x_in_TeV = buf3 * F_over_x_in_TeV

    print('FACT '+str(source)+', power-law fit to data:')
    print("Chi^2    NDF    alpha   f0    F_>%.2f/TeV    "%(energy_sdf_limit/1000.))
    print("%.3f    %d    %.3f+-%.3f    %.3e+-%.3e    %.3e+-%.3e"%(chi2, NDF, alpha, sig_alpha, f0, sig_f0, F_over_x_in_TeV, sig_F_over_x_in_TeV))

    #return the above values
    return chi2, NDF, alpha, sig_alpha, f0, sig_f0, F_over_x_in_TeV, sig_F_over_x_in_TeV

def log_parabola(E, f0, alpha, beta, E0=1000.0):
    return f0*( E/E0 )**(alpha+beta*np.log10(E/E0))

def power_law( E, f0, alpha, E0=1000.0 ):
    return log_parabola( E, f0=f0, alpha=alpha, beta=0, E0=E0 )

def exponential_cutoff( E, f0, alpha, Ecut, E0=1000.0 ):
    return power_law( E=E, f0=f0, alpha=alpha, E0=E0 )*np.exp( -E/Ecut )

# the integrated event rate, numerically evaluated, in 1/s
def event_rate_assuming_powerlaw( E0, f0, alpha, eff_area, energy_range=[300,30000] ):
    #the trailing ten is calculated from: m^2 -> cm^2 and because 
    #of substitution of the variable from E/TeV -> E/GeV :-> 10000/1000 = 10
    integrand = lambda x: power_law( x, f0=f0, alpha=alpha, E0=E0 )*eff_area[0](np.log10(x))*10.
    return integrate.quad(integrand, energy_range[0], energy_range[1],limit=1000,full_output=1)[0]

# the counts per time assuming power law
# takes t in h
def counts_in_ton_time(f0, alpha, E0, Aeff, energy_range=[300,30000], ton=100.):
    counts_per_second = event_rate_assuming_powerlaw(E0, f0, alpha, Aeff, energy_range)
    return counts_per_second*ton*60.*60.

# a primitive way of calculating the ul
def get_simple_ul(Non, Noff, alpha=0.2):
    sqrt_variance = np.sqrt(Non+alpha*alpha*Noff)
    excess = Non-alpha*Noff
    if excess < 0:
        return 3*sqrt_variance
    else: 
        return excess+3*sqrt_variance

# a complicated way of calculating the ul
def get_complicated_ul(Non, Noff, alpha=0.2):
    print("Non\tNoff\talpha")
    print(Non,"\t",Noff,"\t",alpha)
    print(" ")
    upper_limit = float(input("Please enter lambda_95: "))
    print("you entered:")
    print(upper_limit)
    return upper_limit

# get the upper limit for observations, only if requested 
# calculate it with external program and then pass it to the function.
def upper_limit_on_off(Non, Noff, alpha=0.2, simple=True):
    if simple == True:
        return get_simple_ul(Non, Noff, alpha=alpha)
    else:
        return get_complicated_ul(Non, Noff, alpha=alpha)

def magic_crab_spectrum( E ):
    #Magic, ApJ 674
    erange = [60,15000]
    if erange[0] <= E <= erange[1]:
        return log_parabola( E, f0=6.0e-10, alpha=-2.31, beta=-0.26, E0=300.0 )
    return 0.

def veritas_crab_spectrum( E ):
    #Veritas, K. Meagher, ICRC2015: "Six zears of VERITAS observations of the Crab nebula"
    erange = [100,40000]
    if erange[0] <= E <= erange[1]:
        return log_parabola( E, f0=3.75e-11, alpha=-2.467, beta=-0.16, E0=1000.0 )
    return 0.

def hegra_crab_spectrum( E ):
    #HEGRA, 2004, Aharonian et al., ApJ 614:897-913, Oct 20
    erange = [500,70000]
    if erange[0] <= E <= erange[1]:
        return power_law( E, f0=2.83e-11, alpha=-2.62, E0=1000.0 )
    return 0.

def hess_crab_spectrum( E ):
    #HESS, Aharonian et al., A&A 457, 899–915 (2006)
    erange = [500,30000]
    if erange[0] <= E <= erange[1]:
        return exponential_cutoff( E, f0=3.76e-11, alpha=-2.39, Ecut=14300.0 , E0=1000.0 )
    return 0.

def plot_power_law(f0, alpha, E0=1000.0, xlim=[30,80000],linestyle='k:',label=''):
    e_x=10**np.arange( np.log10(xlim[0]),np.log10(xlim[1])+0.05,0.05 )
    e_y = power_law( e_x, f0, alpha, E0=1000.0 )

    plt.plot( np.log10(e_x), e_y, linestyle, label=label )
    plt.semilogy()

    plt.xlabel("E / GeV")
    plt.ylabel("dN/dE / [(cm$^2$ s TeV)$^{-1}$]")

def plot_measured_crab_spectra(xlim=[30,80000],all=False):

    e_x=10**np.arange( np.log10(xlim[0]),np.log10(xlim[1])+0.05,0.05 )
    e_y_magic = [ magic_crab_spectrum( i ) for i in e_x ]
    e_y_hess = [ hess_crab_spectrum( i ) for i in e_x ]
    e_y_veritas = [ veritas_crab_spectrum( i ) for i in e_x ]
    e_y_hegra = [ hegra_crab_spectrum( i ) for i in e_x ]

    plt.plot( e_x, e_y_magic, 'k--', label='Crab Magic' )
    plt.plot( e_x, np.array(e_y_magic)*8./10., 'k--', alpha=0.8, label='0.8 C.U' )
    plt.plot( e_x, np.array(e_y_magic)*6./10., 'k--', alpha=0.6, label='0.6 C.U' )
    plt.plot( e_x, np.array(e_y_magic)*4./10., 'k--', alpha=0.4, label='0.4 C.U' )
    plt.plot( e_x, np.array(e_y_magic)*2./10., 'k--', alpha=0.2, label='0.2 C.U' )
    
    if all:
        plt.plot( e_x, e_y_hess, 'k:', label='Crab H.E.S.S.' )
        plt.plot( e_x, e_y_veritas, 'k-.', label='Crab Veritas' )

    plt.loglog()
    plt.xlim(xlim)

    plt.legend()
    plt.title("Crab Nebula")
    plt.xlabel("E / GeV")
    plt.ylabel("dN/dE / [(cm$^2$ s TeV)$^{-1}$]")

def get_energy_flux_points_from_file_in_GeV_over_TeV_per_cm2_s(filepath):
    conversion_factor_energy = 1.
    conversion_factor_flux = 1.

    if "veritas" in filepath[-40:]:
        conversion_factor_energy = 1./1000.
        conversion_factor_flux = 1./1000000.

    data = read_from_file(filepath)
    data[:,0] *= conversion_factor_energy
    data[:,1] *= conversion_factor_flux
    return data

def get_sed_errorbar_plot(upper_filepath, middle_filepath, lower_filepath):
    upper_data = get_energy_flux_points_from_file_in_GeV_over_TeV_per_cm2_s(upper_filepath)
    middle_data = get_energy_flux_points_from_file_in_GeV_over_TeV_per_cm2_s(middle_filepath)
    lower_data = get_energy_flux_points_from_file_in_GeV_over_TeV_per_cm2_s(lower_filepath)

    x = middle_data[:,0]
    y = middle_data[:,1]
    upper_relative_error = upper_data[:,1]-middle_data[:,1] 
    lower_relative_error = middle_data[:,1]-lower_data[:,1] 
    y_err = np.vstack((upper_relative_error,lower_relative_error))

    return x, y, y_err

def get_magic_crab_pulsar_sed():
    base = sed_data_relative_path
    file_paths= sed_magic_data_files
    p1_energy, p1_flux, p1_dflux = get_sed_errorbar_plot(base+file_paths[0],base+file_paths[1],base+file_paths[2])
    p2_energy, p2_flux, p2_dflux = get_sed_errorbar_plot(base+file_paths[3],base+file_paths[4],base+file_paths[5])

    # calculate the sum of the two magic phase fluxes
    energy = p2_energy.copy()
    flux = p2_flux.copy()
    dflux = p2_dflux.copy()
    for index in range(len(p2_energy)):
        if index >= len(p1_energy):
            break
        energy[index] += p1_energy[index]
        energy[index] /= 2.
        flux[index] += p1_flux[index]
        dflux[0][index] = np.sqrt(dflux[0][index]**2 + p1_dflux[0][index]**2)
        dflux[1][index] = np.sqrt(dflux[1][index]**2 + p1_dflux[1][index]**2)
    
    return energy, flux, dflux

def get_fermi_crab_pulsar_sed():
    # TODO: get the Fermi-LAT P1/P2 data from tha MAGIC publication! 
    # At the moment these P1 / P2 data are only a copy of the full pulsar SED
    # including the bridge from the 2.nd catalog of Fermi-LAT pulsars
    base = sed_data_relative_path
    file_paths= sed_fermi_data_files
    p1_energy, p1_flux, p1_dflux = get_sed_errorbar_plot(base+file_paths[0],base+file_paths[1],base+file_paths[2])
    p2_energy, p2_flux, p2_dflux = get_sed_errorbar_plot(base+file_paths[3],base+file_paths[4],base+file_paths[5])

    # calculate the sum of the two magic phase fluxes
    energy = p2_energy.copy()
    flux = p2_flux.copy()
    dflux = p2_dflux.copy()
    for index in range(len(p2_energy)):
        if index >= len(p1_energy):
            break
        energy[index] += p1_energy[index]
        energy[index] /= 2.
        flux[index] += p1_flux[index]
        dflux[0][index] = np.sqrt(dflux[0][index]**2 + p1_dflux[0][index]**2)
        dflux[1][index] = np.sqrt(dflux[1][index]**2 + p1_dflux[1][index]**2)
    
    return energy, flux, dflux

def get_veritas_crab_pulsar_sed():
    base = sed_data_relative_path
    file_paths= sed_veritas_data_files
    return get_sed_errorbar_plot(base+file_paths[0],base+file_paths[1],base+file_paths[2])

def get_crab_nebula_wide_sed():
    base = nebula_data_relative_path
    file = nebula_files 
    data = get_energy_flux_points_from_file_in_GeV_over_TeV_per_cm2_s(base+file[0])
    return data[:,0], data[:,1]

def plot_crab_nebula_sed():
    nebula_energy, nebula_flux = get_crab_nebula_wide_sed()
    plt.plot(nebula_energy, nebula_flux, 'k--', alpha=1, label='Crab Nebula JHEAp 5-6 (2015)')

def plot_crab_nebula_sed_comparison(markers=[0.1,0.01,0.001]):
    nebula_energy, nebula_flux = get_crab_nebula_wide_sed()
    plt.plot(nebula_energy, nebula_flux, 'k--', alpha=1, label='Crab Nebula JHEAp 5-6 (2015)')
    x_annotation = 1./3.
    y_annotation = 2e-12
    for i in range(len(markers)):
        plt.text(x_annotation, y_annotation, str(markers[i])+' C.U.',  alpha=1.-float(i)/len(markers), 
            color='black', bbox=dict(facecolor='white', edgecolor='none'))
        plt.plot(nebula_energy, nebula_flux*markers[i], 'k--', alpha=1.-float(i)/len(markers))
        x_annotation = x_annotation*10
        y_annotation = y_annotation/6

def plot_measured_crab_pulsar_spectra(xlim=[0.1,2000]):
    magic_energy, magic_flux, magic_dflux = get_magic_crab_pulsar_sed()
    veritas_energy, veritas_flux, veritas_dflux = get_veritas_crab_pulsar_sed()
    fermi_energy, fermi_flux, fermi_dflux = get_fermi_crab_pulsar_sed()

    plt.errorbar( 
        magic_energy, 
        magic_flux,
        magic_dflux,
        fmt='k+',
        markersize=5,
        label='Magic A&A 585 (2015)')

    plt.errorbar( 
        veritas_energy, 
        veritas_flux,
        veritas_dflux,
        fmt='k^',
        markersize=4,
        label='Veritas Science 334 (2011)')

    plt.errorbar( 
        fermi_energy, 
        fermi_flux,
        fermi_dflux, 
        fmt='ko',
        markersize=4,        
        label='Fermi A&A 585 (2015)')

    plt.loglog()
    plt.xlim(xlim)

    plt.legend(loc='lower left')
    plt.title("Crab Pulsar P1+P2 SED")
    plt.xlabel("E / GeV")
    plt.ylabel("E$^2 * $dN/dE / [TeV (cm$^2$ s)$^{-1}$]")

def fit_measured_crab_pulsar_spectra():
    magic_energy, magic_flux, magic_dflux = get_magic_crab_pulsar_sed()
    veritas_energy, veritas_flux, veritas_dflux = get_veritas_crab_pulsar_sed()
    fermi_energy, fermi_flux, fermi_dflux = get_fermi_crab_pulsar_sed()

    # collect measurements for fitting, but only regard the last 3 from fermi
    energies = np.hstack((fermi_energy[-3:],veritas_energy,magic_energy))
    fluxes = np.hstack((fermi_flux[-3:],veritas_flux,magic_flux))
    dfluxes_upper = np.hstack((fermi_dflux[0,-3:],veritas_dflux[0,:],magic_dflux[0,:]))
    dfluxes_lower = np.hstack((fermi_dflux[1,-3:],veritas_dflux[1,:],magic_dflux[1,:]))
    dfluxes = np.vstack((dfluxes_upper, dfluxes_lower))

    sample = fit_power_law_to_sed(energies, fluxes, dfluxes)
    return sample

def plot_contours_from_sample(sample,axis=None,levels=None):
    if levels is None: 
        levels = 1- np.exp(-0.5*(np.arange(1,3.1,1)**2)) # one, two and three sigma in 2D
    
    # save the x and y limits
    if axis is not None:
        x_lim = axis.get_xlim() 
        y_lim = axis.get_ylim() 

    corner.hist2d(sample[:,0], 
        sample[:,1], 
        plot_datapoints=False, 
        plot_density=False,
        no_fill_contours=True,
        ax=axis,
        smooth=True,
        levels=levels)

    if axis is not None:
        axis.set_xlim(x_lim)
        axis.set_ylim(y_lim)

def plot_power_law_sed(f0, alpha, E0=150.0, xlim=[10,5000], linestyle='k:', label='', alpha_plot=1.):
    e_x=10**np.arange( np.log10(xlim[0]),np.log10(xlim[1])+0.05,0.05 )
    e_y = power_law( e_x, f0, alpha, E0=E0 ) * e_x * e_x / 1e6
    plt.plot( e_x, e_y, linestyle, label=label, alpha=alpha_plot)

def plot_power_law_into_fitted_sed(fit_samples, E0=150.0, xlim=[10,5000], linestyle='k:', label=''):
    f0_mcmc, alpha_mcmc = zip(*np.percentile(fit_samples, [16, 50, 84], axis=0))
    
    for f0, alpha in fit_samples[np.random.randint(len(fit_samples), size=100)]:
        plot_power_law_sed(f0, alpha, E0=E0, xlim=xlim, linestyle='k', label=label, alpha_plot=0.025)

    plot_power_law_sed(f0_mcmc[1], alpha_mcmc[1], E0=E0, xlim=xlim, linestyle='r', label=label, alpha_plot=0.7)

def plot_crab_pulsar_nebula_comparison(xlim=[0.1,6000],markers=[0.1,0.01,0.001]):
    plot_crab_nebula_sed_comparison(markers)
    plot_measured_crab_pulsar_spectra(xlim=xlim)
    fit_samples = fit_measured_crab_pulsar_spectra()
    plot_power_law_into_fitted_sed(fit_samples, xlim=[10,xlim[1]], linestyle='k')
    #corner.corner(fit_samples, labels=["$f_0$", "$\\alpha$"])
    return fit_samples

# this is the the ln likelihood of a linear fit in 
# double logarithmic scale (energy, flux) in order to get
# power law model parameters f0 and alpha
def ln_likelihood(energies, fluxes, dfluxes, E0, f0, alpha):
    #print('E0, f0, alpha')    
    #print(E0, f0, alpha)

    sigma_n = np.log10(dfluxes/fluxes + 1.)

    y_n = np.log10(fluxes)
    x_n = np.log10(energies)
    a = alpha
    b = np.log10(f0) - np.log10(E0)*alpha

    tail = np.log(2.*np.pi*sigma_n*sigma_n)
    
    summand_part1 = y_n-a*x_n-b
    summand = summand_part1 * summand_part1 / sigma_n / sigma_n + tail

    #print('summand',summand)
    #print('ln likelihood ',-np.sum(summand)/2.)

    return -np.sum(summand)/2.

# flat prior with physics boundaries:
# f0 must be positive, alpha must ensure normalizable flux
def ln_prior(f0,alpha):
    if f0 < 0:
        return -np.inf
    if alpha > -1:
        return -np.inf
    return 0.

E0 = 150. / 1000.

# combine prioir and likelihood for calculating the posterior
def ln_posterior(theta, energies, fluxes, dfluxes):
    f0, alpha = theta
    a = ln_prior(f0,alpha)
    if a == -np.inf:
        return a

    b = ln_likelihood(energies, fluxes, dfluxes, E0=E0, f0=f0, alpha=alpha)
    return a + b

# this method performs a mcmc fit of a power law to the given data points
# input data:   energy_data / GeV
#               flux_data / TeV (cm^2 s)^-1
#               dflux_data / TeV (cm^2 s)^-1
#               E0 / GeV
#               f0_start / (TeV cm^2 s)^-1
def fit_power_law_to_sed(energy_data, flux_data, dflux_data, f0_start=4.2e-11, alpha_start=-3.8):
    # first bring the data points into non skewed form (all energies to TeV)
    energies = energy_data/1000.
    fluxes = flux_data/energies/energies

    # make the mean of the dflux_data
    dfluxes = (dflux_data[0]+dflux_data[1])/2.
    dfluxes = dfluxes/energies/energies

    # same setup as in tutorial:
    ndim, nwalkers = 2, 50
    nsteps, nburn = 2000, 1000

    starting_guesses = np.random.rand(nwalkers, ndim)
    starting_guesses[:, 0] = np.random.normal(f0_start, f0_start/10, nwalkers) 
    starting_guesses[:, 1] = np.random.normal(alpha_start, np.abs(alpha_start)/10, nwalkers) 

    sampler = emcee.EnsembleSampler(nwalkers, ndim, ln_posterior, args=(energies, fluxes, dfluxes))
    sampler.run_mcmc(starting_guesses, nsteps)

    sample = sampler.chain  # shape = (nwalkers, nsteps, ndim)
    sample = sampler.chain[:, nburn:, :].reshape(-1, 2)

    return sample

# assuming power law
def plot_lines_of_constant_measured_counts(
    Aeff,
    threshold,
    f0_limits = [2.6e-11, 5.4e-11], 
    alpha_limits = [-3.45, -2.6], 
    E0=150, 
    energy_range=[300,30000],
    ton=100.,
    pixels_per_line=30,
    plot_color=False):
    
    f0_stepsize = (f0_limits[1]-f0_limits[0])/pixels_per_line
    alpha_stepsize = (alpha_limits[1]-alpha_limits[0])/pixels_per_line

    f0_stepsize = f0_stepsize+f0_stepsize*1e-9
    alpha_stepsize = alpha_stepsize+alpha_stepsize*1e-9

    f0_buf = np.arange(f0_limits[0], f0_limits[1], f0_stepsize)
    alpha_buf = np.arange(alpha_limits[1], alpha_limits[0], -alpha_stepsize)
    F0, Alpha = np.meshgrid(f0_buf, alpha_buf)
    
    N_per_ton = np.array([ [ counts_in_ton_time(
        F0[i,j],
        Alpha[i,j],
        E0=E0,
        Aeff=Aeff,
        energy_range=energy_range,
        ton=ton) for j in range(pixels_per_line) ] for i in range(pixels_per_line) ])

    # figure = plt.figure()
    if plot_color:
        im = plt.imshow(N_per_ton, cmap=plt.cm.YlOrRd, extent=(F0[0][0], F0[0][-1], Alpha[-1][0], Alpha[0][0]),aspect='auto')
    cset = plt.contour(F0,
        Alpha,
        N_per_ton, 
        10, 
        linestyles = 'dashed',
        linewidths=1,
        colors='k')
    plt.clabel(cset, inline=True, fmt='%1.1f', fontsize=10)
    if plot_color:
        plt.colorbar(im)  

    plt.title('signal counts per %1.1f h, E$_0$=%1.1f GeV assuming power law'%(ton,E0))
    plt.xlabel('$f_0$ / [(cm$^2$ s TeV)$^{-1}$]')
    plt.ylabel('$\\alpha$')
    return F0,Alpha,N_per_ton

def round_down_100(x):
    return int(np.floor(x/100))*100

# assuming power law
def plot_lines_of_detection_threshold(
    Aeff,
    threshold,
    f0_limits = [2.6e-11, 5.4e-11], 
    alpha_limits = [-3.45, -2.6], 
    E0=150, 
    energy_range=[300,30000],
    ton=100.,
    pixels_per_line=31,
    plot_color=False,
    ):
    
    f0_stepsize = (f0_limits[1]-f0_limits[0])/pixels_per_line
    alpha_stepsize = (alpha_limits[1]-alpha_limits[0])/pixels_per_line

    f0_stepsize = f0_stepsize+f0_stepsize*1e-9
    alpha_stepsize = alpha_stepsize+alpha_stepsize*1e-9

    f0_buf = np.arange(f0_limits[0], f0_limits[1], f0_stepsize)
    alpha_buf = np.arange(alpha_limits[1], alpha_limits[0], -alpha_stepsize)
    F0, Alpha = np.meshgrid(f0_buf, alpha_buf)
    
    N_per_ton = np.array([ [ counts_in_ton_time(
        F0[i,j],
        Alpha[i,j],
        E0=E0,
        Aeff=Aeff,
        energy_range=energy_range,
        ton=ton) for j in range(pixels_per_line) ] for i in range(pixels_per_line) ])

    # reshape the
    threshold_constant = threshold*np.sqrt(ton)
    time_to_detection = (threshold_constant / N_per_ton)**2

    # figure = plt.figure()
    if plot_color:
        im = plt.imshow(time_to_detection, cmap=plt.cm.YlOrRd, extent=(F0[0][0], F0[0][-1], Alpha[-1][0], Alpha[0][0]),aspect='auto')
    
    starting_level = round_down_100(ton)
    if starting_level == 0:
        starting_level = 400
    levels = [ 400*np.power(2,i-1) for i in range(6) ]

    cset = plt.contour(F0,
        Alpha,
        time_to_detection, 
        levels = levels, 
        linestyles = 'dashed',
        linewidths=1,
        colors='k')
    plt.clabel(cset, inline=True, fmt='%1.1f', fontsize=10)
    if plot_color:
        plt.colorbar(im)  

    plt.title('time to detection in h, E$_0$=%1.1f GeV assuming power law'%E0)
    plt.xlabel('$f_0$ / [(cm$^2$ s TeV)$^{-1}$]')
    plt.ylabel('$\\alpha$')
    return F0,Alpha,time_to_detection

def get_integral_limit_slope_independent(E, lambda_95, E0, Aeff, ton, integral_energy_range=[300,30000]):
    log10_f0_start = np.log10(4.0e-11)
    alpha_start = -3.0

    # first define function to be minimized
    fun = lambda x: np.log10(power_law( E, f0=10**x[0], alpha=x[1], E0=E0 ))*(-1)

    cons = ({'type': 'ineq',
     'fun': lambda x: lambda_95 - counts_in_ton_time(f0=10**x[0], 
        alpha=x[1], 
        E0=E0, 
        Aeff=Aeff, 
        energy_range=integral_energy_range, 
        ton=ton)})

    # variables bounds (f0, alpha):
    bnds = ((np.log10(4e-20), np.log10(4e-5)), (-5., -1.))

    #run the minimizer
    res = minimize(fun, (log10_f0_start, alpha_start), method='SLSQP', bounds=bnds, constraints=cons)
    res['x'][0] = 10**res['x'][0]
    return res

def plot_integral_limit_slope_independent_sed(Aeff, 
    ton, 
    E0, 
    print_time=True,
    lambda_95=212.897, 
    xlim=[600,6000], 
    integral_energy_range=[300,30000], 
    linestyle='k:', 
    label='',
    alpha_plot=1.0):

    e_x = 10**np.arange( np.log10(xlim[0]),np.log10(xlim[1])+0.05,0.05 )
    e_y = []
    
    for energy in e_x:
        res = get_integral_limit_slope_independent(E=energy, 
            lambda_95=lambda_95, 
            E0=E0, 
            Aeff=Aeff, 
            ton=ton,
            integral_energy_range=integral_energy_range)
        f0 = res['x'][0]
        alpha = res['x'][1]
        cnt = counts_in_ton_time(f0, alpha, ton=ton, E0=E0, Aeff=Aeff, energy_range=integral_energy_range)
        
        # print("ton: ", ton, "E0: ", E0, "integral_energy_range: ", integral_energy_range)
        # print(f0, alpha, "cnts: ", cnt)
        e_y.append(power_law( energy, f0, alpha, E0=E0 ) * energy * energy / 1e6)
        # print('calculating energy: ',energy)
    e_y = np.array(e_y)

    if print_time:
        plt.text( e_x[0]*0.8, e_y[0]*1.2, '%.1f h'%ton,  alpha=alpha_plot, 
            color='black', bbox=dict(facecolor='white', edgecolor='none'))
    plt.plot( e_x, e_y, linestyle, label=label, alpha=alpha_plot)
