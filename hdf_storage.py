
""" a function to retrieve the data

possibly using a cached version, if it is not older than *max_age_hours*
"""
import os
import datetime
import pandas as pd

from tqdm import tqdm
from inspecting import (
        get_corsika_evts,
        get_faccttols_evts,
        get_faccttols_data_evts,
        prepare_df,
        drop_nan_and_inf_inplace,
        all_files_matching
        )

max_age_hours = 100000

this_files_path = os.path.dirname(os.path.realpath(__file__))
cache_path = os.path.join(this_files_path, "store.h5")

def cache_age_in_hours():
    st=os.stat(cache_path)
    mtime=datetime.datetime.fromtimestamp(st.st_mtime)
    delta = datetime.datetime.now() - mtime
    age_in_hours = delta.total_seconds() / 3600

    return age_in_hours

def cache_exists():
    return os.path.exists(cache_path)

def clear_cache():
    os.remove(cache_path)

def get_events_df(cfg,recache):
    if not cache_exists() or cache_age_in_hours() >= max_age_hours or recache:
        print("recaching...")
        stuff = inspect_stuff(cfg)
        store_stuff(*stuff)
    else:
        print("reading events from cached file...")
        stuff = read_from_cache()
    return stuff

def inspect_stuff(cfg):
    data_evts = get_faccttols_data_evts(cfg)
    prepare_df( data_evts )
    drop_nan_and_inf_inplace( data_evts )

    corsika_evts = get_corsika_evts(cfg)
    facttools_evts = get_faccttols_evts(cfg)
    facttools_evts = corsika_evts.join(facttools_evts, how='inner')

    # prepare the facttools df for the energy-regression/separation (by adding columns and eliminating infs and other nans)
    prepare_df( facttools_evts )
    drop_nan_and_inf_inplace( facttools_evts )

    corsika_evts["survived facttools"] = False
    corsika_evts.loc[facttools_evts.index, "survived facttools"] = True

    return corsika_evts, facttools_evts, data_evts

def store_stuff(corsika_evts, facttools_evts, data_evts):
    store = pd.HDFStore(cache_path)
    store["corsika_evts"] = corsika_evts
    drop_nan_and_inf_inplace( facttools_evts )
    store["facttools_evts"] = facttools_evts
    drop_nan_and_inf_inplace( data_evts )
    store["data_evts"] = data_evts
    store.close()

def read_from_cache():
    with pd.HDFStore(cache_path) as store:
        return store["corsika_evts"], store["facttools_evts"], store["data_evts"]

def read_hdf5_file(path,maxreuse):
    if maxreuse == -1:
        with pd.HDFStore(path) as store:
            return store["evts"]

    #else apply the cuts prescribed in the cfg
    with pd.HDFStore(path) as store:
        return store.select("evts",store["evts"]['reuse number']<=maxreuse)

def get_df_from_list_of_hdf5_files(file_list,maxreuse=-1):
    df_list = []
    one_file = file_list[0]
    for path in tqdm(file_list, desc="inspecting hdf5 files, starting with "+one_file):
        df_list.append(read_hdf5_file(path,maxreuse))

    evts = pd.concat(df_list, ignore_index=True)
    return evts

def get_events_df_from_hdf5(cfg):

    print("reading events from hdf5 files...")
    
    corsika_paths = all_files_matching(cfg['mc_cherenkov_photons']['path'])
    facttools__paths = all_files_matching(cfg['shower_paramters']['path'])
    data_paths = all_files_matching(cfg['facttools_processed_telescope_data']['path'])

    print('get corsika files')
    corsika_evts = get_df_from_list_of_hdf5_files(corsika_paths,maxreuse=cfg['max_reuse'])
    print('get facttools files')
    facttools_evts_raw = get_df_from_list_of_hdf5_files(facttools__paths,maxreuse=cfg['max_reuse'])
    print('get data files')
    data_evts_raw = get_df_from_list_of_hdf5_files(data_paths)

    #join the missing data to the facttools dataframe
    print('join the missing data to the facttools dataframe')
    facttools_evts_raw = pd.merge(
        corsika_evts.reset_index(), 
        facttools_evts_raw.reset_index(), 
        on=['run number','event number','reuse number'], 
        how='inner')
    
    print('set index corsika df')
    corsika_evts = corsika_evts.set_index(['run number','event number','reuse number'])
    print('set index facttools df')
    facttools_evts_raw = facttools_evts_raw.set_index(['run number','event number','reuse number'])
    print('set index data df')
    data_evts_raw = data_evts_raw.set_index(['NIGHT','RUNID','EventNum'])

    print('prepare the facttools df for the energy-regression/separation (by adding columns and eliminating infs and other nans)')
    prepare_df( facttools_evts_raw )
    prepare_df( data_evts_raw )
    drop_nan_and_inf_inplace( facttools_evts_raw )
    drop_nan_and_inf_inplace( data_evts_raw )

    print('reading done')
    return corsika_evts, facttools_evts_raw, data_evts_raw