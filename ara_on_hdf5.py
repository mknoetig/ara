"""Ara main program, creates models and the effective area

Usage:
  ara.py [--noshow] <configuration_file> <out_path>
  ara.py (-h | --help)
  ara.py --version

Options:
  -h --help     Show this screen.
  --version     Show version.
"""
from __future__ import absolute_import, print_function, division

import os
import json
import sys
import pickle

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

from tqdm import tqdm
from docopt import docopt

# ----- the start of the main -----------
from hdf_storage import get_events_df_from_hdf5
from sql import get_runinfo
import inspecting
import training_and_test as T_T
import plotting
import statistics
from sources import *

if __name__ == "__main__":

    #read config and initialize program
    arguments = docopt(__doc__, version='Ara 1')
    cfg = inspecting.read_config_file(arguments["<configuration_file>"])
    corsika_evts, facttools_evts_raw, data_evts_raw = get_events_df_from_hdf5(cfg)

    noshow = arguments["--noshow"]

    #apply default mc missmatch cuts
    print('applying default mismatch cuts to data')
    data_evts = T_T.apply_default_mc_missmatch_cuts( data_evts_raw, cfg )
    print('applying default mismatch cuts to mc')
    facttools_evts = T_T.apply_default_mc_missmatch_cuts( facttools_evts_raw, cfg )

    # create selection columns for test and training sets in the fully classified event dataframe
    print('define test and training set')
    T_T.define_test_and_training_set( facttools_evts, cfg )

    #disp regression, returns the disp regressor
    '''disp_regressor,sklearn_disp_mapper = T_T.train_and_apply_disp_regressor( facttools_evts, cfg )
    '''

    # energy regression, return the regressors (for each pid)
    energy_regressors, sklearn_energy_mappers, pid_set = T_T.train_and_apply_energy_regressors( facttools_evts, cfg )

    # gamma - proton classifier, electron - proton classifier, ...
    pcas,classifiers,sklearn_class_mappers,pid_combos = T_T.train_and_apply_classifiers( facttools_evts, cfg )

    # cut in gamma ness
    facttools_evts_gamma_cut = facttools_evts.loc[facttools_evts['ARA_pid_1_to_14_ness']>cfg['analysis_cuts']['gamma_proton_ness_cut'] ].copy()

    # cut in theta
    facttools_evts_theta_cut = facttools_evts_gamma_cut.loc[
    facttools_evts_gamma_cut['Theta']*facttools_evts_gamma_cut['Theta']*plotting.FACT_deg_per_mm**2<cfg['analysis_cuts']['theta_2_cut']
    ].copy()

    # apply the regressors and classifiers to the data
    data_evts = T_T.apply_energy_regressors_to_data(data_evts,energy_regressors, sklearn_energy_mappers, pid_set)
    data_evts = inspecting.recast_columns_to_32( data_evts ) #recast the 64bit columns to save space 
    data_evts = T_T.apply_classifiers_to_data(data_evts,classifiers,pcas,sklearn_class_mappers,pid_combos)
    data_evts = inspecting.recast_columns_to_32( data_evts ) #recast the 64bit columns to save space 

    #get the data from the rundb and join it to the data_evts
    runinfo = get_runinfo()
    runinfo = inspecting.drop_db_columns( runinfo,cfg ) 
    runinfo.rename(columns={'fNight': 'NIGHT', 'fRunID': 'RUNID'}, inplace=True)
    runinfo = inspecting.recast_columns_to_32(runinfo)
    runinfo = runinfo.set_index(['NIGHT','RUNID'])   

    #join on the common index
    data_evts=data_evts.reset_index('EventNum')
    data_evts=data_evts.join(runinfo, how='inner')
    data_evts=data_evts.set_index('EventNum', append=True).reorder_levels(['NIGHT', 'RUNID', 'EventNum']).sortlevel()

    # update the surviving columns cuts
    print('updating survived cut column')
    T_T.update_survived_cut_column(corsika_evts, facttools_evts_raw , 1, "survivied_facttools", cfg )
    T_T.update_survived_cut_column(corsika_evts, facttools_evts , 2, "survivied_mc_missmatch_cuts", cfg )
    T_T.update_survived_cut_column(corsika_evts, facttools_evts_gamma_cut, 3, "survivied_gamma_cut", cfg )
    T_T.update_survived_cut_column(corsika_evts, facttools_evts_theta_cut, 4, "survivied_theta_cut", cfg )

    # ---------- prepare output --------------------------

    #make the out dir if it does not exist
    if not os.path.exists(arguments["<out_path>"]):
        os.makedirs(arguments["<out_path>"])
    #make the plots out dir if it does not exist
    if not os.path.exists(arguments["<out_path>"]+'/plots/'):
        os.makedirs(arguments["<out_path>"]+'/plots/')

    # ---------- plot migration matrix, energy est error, ... ------------------
    if not noshow:
        plt.ion()
    
    fig1_1 = plt.figure(figsize=(8, 12))
    ax1_1_1 = fig1_1.add_subplot(211)
    hist1_pars = plotting.plot_Migration_matrix(1,facttools_evts)
    plt.title('$\gamma$ ARA Migration Matrix')

    ax1_1_2 = fig1_1.add_subplot(212,sharex=ax1_1_1)
    plotting.plot_energy_resolution( 1,ax1_1_2,facttools_evts,hist1_pars )
    plotting.plot_energy_error( 1,ax1_1_2,facttools_evts,hist1_pars )
    plt.ylabel(("rel. Energy error"), fontsize=14)
    plt.xlabel("log$_{10}$(E$_{true}$ /GeV)", fontsize=14)

    plt.tight_layout()
    plt.savefig(arguments["<out_path>"]+'/plots/fig1_1.png', bbox_inches='tight', dpi=300)
    plt.savefig(arguments["<out_path>"]+'/plots/fig1_1.pdf', bbox_inches='tight')

    fig1_2 = plt.figure(figsize=(8, 12))
    ax1_2_1 = fig1_2.add_subplot(211)
    hist2_pars = plotting.plot_Migration_matrix(14,facttools_evts)
    plt.title('proton ARA migration matrix')

    ax1_2_2 = fig1_2.add_subplot(212,sharex=ax1_2_1)
    plotting.plot_energy_resolution( 14,ax1_2_2,facttools_evts,hist2_pars )
    plotting.plot_energy_error( 14,ax1_2_2,facttools_evts,hist2_pars )
    plt.ylabel(("rel. Energy error"), fontsize=14)
    plt.xlabel("log$_{10}$(E$_{true}$ /GeV)", fontsize=14)

    plt.tight_layout()
    plt.savefig(arguments["<out_path>"]+'/plots/fig1_2.png', bbox_inches='tight', dpi=300)
    plt.savefig(arguments["<out_path>"]+'/plots/fig1_2.pdf', bbox_inches='tight')
    
    fig2_1 = plt.figure()
    plotting.plot_pid_to_pid_ness_histo( facttools_evts, 1, 14 )
    plt.tight_layout()
    
    plt.savefig(arguments["<out_path>"]+'/plots/fig2_1.png', bbox_inches='tight', dpi=300)
    plt.savefig(arguments["<out_path>"]+'/plots/fig2_1.pdf', bbox_inches='tight')

    fig2_2 = plt.figure()
    plotting.plot_pid_to_pid_ness_survival( facttools_evts, 1, 14 )
    plt.tight_layout()
    
    plt.savefig(arguments["<out_path>"]+'/plots/fig2_2.png', bbox_inches='tight', dpi=300)
    plt.savefig(arguments["<out_path>"]+'/plots/fig2_2.pdf', bbox_inches='tight')

    fig2_3 = plt.figure()
    plotting.plot_roc_curve(facttools_evts, 1, 14)
    plt.tight_layout()

    plt.savefig(arguments["<out_path>"]+'/plots/fig2_3.png', bbox_inches='tight', dpi=300)
    plt.savefig(arguments["<out_path>"]+'/plots/fig2_3.pdf', bbox_inches='tight')

    fig3_1 = plt.figure()
    ax6 = plt.gca()
    plotting.plot_effective_area(plotting.effective_area(corsika_evts,pid=14,cut_name='survivied_facttools',cut_map=T_T.cut_map),'survivied_facttools')
    plotting.plot_effective_area(plotting.effective_area(corsika_evts,pid=14,cut_name='survivied_mc_missmatch_cuts',cut_map=T_T.cut_map),'survivied_mc_missmatch_cuts')
    plotting.plot_effective_area(plotting.effective_area(corsika_evts,pid=14,cut_name='survivied_gamma_cut',cut_map=T_T.cut_map),'survivied_gamma_cut')
    plotting.plot_effective_area(plotting.effective_area(corsika_evts,pid=14,cut_name='survivied_theta_cut',cut_map=T_T.cut_map),'survivied_theta_cut')
    plt.tight_layout()
    plt.savefig(arguments["<out_path>"]+'/plots/fig3_1.png', bbox_inches='tight', dpi=300)
    plt.savefig(arguments["<out_path>"]+'/plots/fig3_1.pdf', bbox_inches='tight')

    fig3_2 = plt.figure()
    ax5 = plt.gca()
    plotting.plot_effective_area(plotting.effective_area(corsika_evts,pid=1,cut_name='survivied_facttools',cut_map=T_T.cut_map),'survivied_facttools')
    plotting.plot_effective_area(plotting.effective_area(corsika_evts,pid=1,cut_name='survivied_mc_missmatch_cuts',cut_map=T_T.cut_map),'survivied_mc_missmatch_cuts')
    plotting.plot_effective_area(plotting.effective_area(corsika_evts,pid=1,cut_name='survivied_gamma_cut',cut_map=T_T.cut_map),'survivied_gamma_cut')
    a_eff_after_cuts = plotting.effective_area(corsika_evts,pid=1,cut_name='survivied_theta_cut',cut_map=T_T.cut_map)
    plotting.plot_effective_area(a_eff_after_cuts,'survivied_theta_cut')
    #add reference plots
    for key, value in cfg['comparison_data']['a_eff'].items():
        data = np.loadtxt(value[0])
        plt.plot(data[:,0],data[:,1],color=value[1],linestyle=value[2],markersize=0,label=key)
    plt.legend(loc='best')

    plt.tight_layout()
    plt.savefig(arguments["<out_path>"]+'/plots/fig3_2.png', bbox_inches='tight', dpi=300)
    plt.savefig(arguments["<out_path>"]+'/plots/fig3_2.pdf', bbox_inches='tight')

    fig4 = plt.figure(figsize=(17,10))
        
    ax10 = fig4.add_subplot(221)
    ret = plotting.plot_theta_2_histo( facttools_evts,1,normed=False )
    _ = plotting.plot_theta_2_histo( facttools_evts_gamma_cut,1,normed=False,annotation="after $\gamma$-cut" )
    #add reference plots
    for key, value in cfg['comparison_data']['theta_2'].items():
        data = np.loadtxt(value[0])
        plt.plot(data[:,0],data[:,1]*ret[0][0],color=value[1],linestyle=value[2],markersize=0,label=key)
    plt.xlim(ret[1][0],ret[1][-1])
    plt.legend(loc='best')

    ax11 = fig4.add_subplot(222)
    plotting.plot_theta_2_histo( facttools_evts,14,normed=False )
    plotting.plot_theta_2_histo( facttools_evts_gamma_cut,14,normed=False,annotation="after $\gamma$-cut" )
    
    ax12 = fig4.add_subplot(223)
    plotting.plot_theta_2_cdf( facttools_evts,1 )
    plotting.plot_theta_2_cdf( facttools_evts_gamma_cut,1,annotation="after $\gamma$-cut"  )

    plotting.plot_theta_2_cdf( facttools_evts,14 )
    plotting.plot_theta_2_cdf( facttools_evts_gamma_cut,14,annotation="after $\gamma$-cut"  )

    ax13 = fig4.add_subplot(224)
    plotting.plot_theta_2_roc( facttools_evts, 1, 14, print_chance=False)
    plotting.plot_theta_2_roc( facttools_evts_gamma_cut, 1, 14, annotation="after $\gamma$-cut")

    plt.tight_layout()
    plt.savefig(arguments["<out_path>"]+'/plots/fig4.png', bbox_inches='tight', dpi=300)
    plt.savefig(arguments["<out_path>"]+'/plots/fig4.pdf', bbox_inches='tight')

    '''fig5 = plt.figure(figsize=(5,14))

    ax14 = fig5.add_subplot(311)
    plotting.plot_disp0_dispRF_error( facttools_evts, 1 )

    ax15 = fig5.add_subplot(312,sharex=ax14)
    plotting.plot_disp0_dispRF_error( facttools_evts_gamma_cut, 1 )

    ax16 = fig5.add_subplot(313,sharex=ax14)
    plotting.plot_disp0_dispRF_error( facttools_evts_theta_cut, 1 )

    plt.tight_layout()'''

    fig6 = plt.figure( figsize=(14,14) )
    fig6.add_subplot(331)
    plotting.plot_data_mc_comparison(facttools_evts, 
                                     data_evts, 
                                     "Size", 
                                     logy=True,log10=True,loc='lower left')
    fig6.add_subplot(332)
    plotting.plot_data_mc_comparison(facttools_evts, 
                                     data_evts, 
                                     "Width", 
                                     logy=False,log10=False,loc='upper right')
    fig6.add_subplot(333)
    plotting.plot_data_mc_comparison(facttools_evts, 
                                     data_evts, 
                                     "Length", 
                                     logy=False,log10=False,loc='upper right')
    fig6.add_subplot(334)
    plotting.plot_data_mc_comparison(facttools_evts, 
                                     data_evts, 
                                     "ARA_pid_1_stdev_Energy_Prediction_in_GeV", 
                                     logy=False,log10=True)
    fig6.add_subplot(335)
    plotting.plot_data_mc_comparison(facttools_evts, 
                                     data_evts, 
                                     "Disp", 
                                     logy=False,log10=False)
    fig6.add_subplot(336)
    plotting.plot_data_mc_comparison(facttools_evts, 
                                     data_evts, 
                                     "Size_over_(Width*Length)", 
                                     logy=True,log10=True,loc='lower left')
    fig6.add_subplot(337)
    plotting.plot_data_mc_comparison(facttools_evts, 
                                     data_evts, 
                                     "Area", 
                                     logy=True,log10=False,loc='upper right')
    fig6.add_subplot(338)
    plotting.plot_data_mc_comparison(facttools_evts, 
                                     data_evts, 
                                     "Concentration_onePixel", 
                                     logy=True,log10=False,loc='upper right')
    fig6.add_subplot(339)
    plotting.plot_data_mc_comparison(facttools_evts, 
                                     data_evts, 
                                     "ConcCore", 
                                     logy=True,log10=False,loc='upper right')
    plt.tight_layout()
    plt.savefig(arguments["<out_path>"]+'/plots/fig6.png', bbox_inches='tight', dpi=300)
    plt.savefig(arguments["<out_path>"]+'/plots/fig6.pdf', bbox_inches='tight')

    fig7 = plt.figure()
    Non, Noff, Nex, SLiMa, ontime, start, stop = plotting.theta_2_histo_data(
        data_evts[data_evts.fZenithDistanceMean<35],
        source='Crab',
        gamma_cut=cfg['analysis_cuts']['gamma_proton_ness_cut'],
        theta2_cut=cfg['analysis_cuts']['theta_2_cut'],
        plotting=True)
    plt.tight_layout()
    print(Non, Noff, Nex, SLiMa, ontime)
    plt.savefig(arguments["<out_path>"]+'/plots/fig7.png', bbox_inches='tight', dpi=300)
    plt.savefig(arguments["<out_path>"]+'/plots/fig7.pdf', bbox_inches='tight')

    fig8 = plt.figure()
    Non, Noff, Nex, SLiMa, ontime, start, stop = plotting.theta_2_histo_data(
        data_evts[data_evts.fZenithDistanceMean<35],
        source='Mrk 421',
        gamma_cut=cfg['analysis_cuts']['gamma_proton_ness_cut'],
        theta2_cut=cfg['analysis_cuts']['theta_2_cut'],
        plotting=True)
    plt.tight_layout()
    print(Non, Noff, Nex, SLiMa, ontime)
    plt.savefig(arguments["<out_path>"]+'/plots/fig8.png', bbox_inches='tight', dpi=300)
    plt.savefig(arguments["<out_path>"]+'/plots/fig8.pdf', bbox_inches='tight')


    fig9 = plt.figure()
    interpol_functions = plotting.get_a_eff_interpol_functions(a_eff_after_cuts)
    plotting.plot_measured_crab_spectra([500,13000],all=False)
    plotting.plot_spectrum(
        data_evts[data_evts.fZenithDistanceMean<35],
        interpol_functions,
        source='Crab',
        gamma_cut=cfg['analysis_cuts']['gamma_proton_ness_cut'],
        theta2_cut=cfg['analysis_cuts']['theta_2_cut'],
        beta=0.5
        )
    plotting.plot_spectrum(
        data_evts[data_evts.fZenithDistanceMean<35],
        interpol_functions,
        source='Mrk 421',
        gamma_cut=cfg['analysis_cuts']['gamma_proton_ness_cut'],
        theta2_cut=cfg['analysis_cuts']['theta_2_cut'],
        beta=0.5
        )
    #plotting.plot_power_law( 2.590e-11, -3.997, xlim=[550, 1900] )
    plt.legend()
    plt.tight_layout()
    plt.savefig(arguments["<out_path>"]+'/plots/fig9.png', bbox_inches='tight', dpi=300)
    plt.savefig(arguments["<out_path>"]+'/plots/fig9.pdf', bbox_inches='tight')
    
    #save the models and the effective area to disk

    energy_models = {
        "energy_regressors": energy_regressors, 
        "sklearn_energy_mappers": sklearn_energy_mappers, 
        "pid_set": pid_set}

    classifier_models = {
        "classifiers": classifiers, 
        "pcas": pcas, 
        "sklearn_class_mappers": sklearn_class_mappers, 
        "pid_combos":pid_combos}

    ara_models_and_A_eff = { 
        "energy_models":energy_models, 
        "classifier_models":classifier_models, 
        "Aeff":interpol_functions, 
        "configuration":cfg }
    
    with open(arguments["<out_path>"]+'/models_and_A_eff.pickle', 'wb') as handle:
        pickle.dump(ara_models_and_A_eff, handle)

    if not noshow:
        plt.show()