#!/bin/env python
"""Convert folder of mc json files into one HDFStore

Usage:
  to_hdfstore.py <cfg> <infolder> <outfile> 
  to_hdfstore.py (-h | --help)
  to_hdfstore.py --version

Options:
  -h --help     Show this screen.
  --version     Show version.
"""
from docopt import docopt
import os
import glob
import pandas as pd
from tqdm import tqdm
import sys
import time
import inspecting

arguments = docopt(__doc__, version='to_hdfstore 0.1')
cfg = inspecting.read_config_file(arguments["<cfg>"])

paths = glob.glob(os.path.realpath(arguments["<infolder>"])+'/*.json')
paths.sort()

hdfstore_filename = arguments["<outfile>"]
if os.path.exists(hdfstore_filename):
    os.remove(hdfstore_filename)

with pd.get_store(hdfstore_filename) as store:

    for path in tqdm(paths):

        events = inspecting.inspect_shower_parameters_run(path,cfg)
        inspecting.drop_nan_and_inf_inplace( events )

        try:
            nrows = store.get_storer('evts').nrows
        except:
            nrows = 0

        #trick to make the index continuous
        events.index = pd.Series(events.index) + nrows
        store.append('evts',events)

store.close()