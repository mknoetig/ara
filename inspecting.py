import glob
from tqdm import tqdm
import json

import numpy as np
import pandas as pd

import py_mmcs_corsika

def read_config_file(path):
    f = open(path)
    cfg = json.load(f)
    f.close()
    return cfg

def recast_columns_to_32( df ):
    df = recast_float64_to_float32(df)
    df = recast_int64_to_int32(df)
    return df

def inspect_mc_cherenkov_photons_run(path, cfg):
    event_getter = py_mmcs_corsika.EventGetter(path)
    run = pd.DataFrame([{**event_getter.RunHeader, **corsika_event.Header} for corsika_event in event_getter])   
    run = correct_XSCAT_variable(run)
    run = run.drop(cfg['mc_cherenkov_photons']['drop_keys'], axis=1)
    run = flatten_lists(run, cfg['mc_cherenkov_photons']['flatten_description'])
    run = recast_columns_to_32(run)
    return run

def inspect_shower_parameters_run(path, cfg):
    with open(path) as f:
        run = pd.DataFrame(json.load(f))
    run = add_corsika_run_number_from_source_string(run)
    run.rename(columns=cfg['shower_paramters']['rename_cols'], inplace=True)
    run = run.drop(cfg['shower_paramters']['drop_keys'], axis=1)
    run = flatten_lists(run)
    run = run.drop(cfg['drop_keys_for_storage'], axis=1)
    run = recast_columns_to_32(run)
    return run

def inspect_data_run(path, cfg):
    with open(path) as f:
        run = pd.DataFrame(json.load(f))
    run = flatten_lists(run)
    run = run.drop(cfg['drop_keys_for_storage'], axis=1)
    run = recast_columns_to_32(run)
    return run

def get_corsika_evts(cfg):
    df_list = []
    for path in tqdm(all_files_matching(cfg['mc_cherenkov_photons']['path']), desc="inspecting corsika"):
        df_list.append(inspect_mc_cherenkov_photons_run(path, cfg))
    df = pd.concat(df_list, ignore_index=True)
    df = df.set_index(['run number','event number','reuse number'])
    return df

def get_faccttols_evts(cfg):
    df_list = []
    for path in tqdm(all_files_matching(cfg['shower_paramters']['path']), desc="inspecting facttols mc"):
        df_list.append(inspect_shower_parameters_run(path, cfg))
    df = pd.concat(df_list, ignore_index=True)
    df = df.set_index(['run number','event number','reuse number'])
    return df

def get_faccttols_data_evts(cfg):
    df_list = []
    for path in tqdm(all_files_matching(cfg['facttools_processed_telescope_data']['path']), desc="inspecting facttols data"):
        df_list.append(inspect_data_run(path, cfg))
    df = pd.concat(df_list, ignore_index=True)
    df = df.set_index(['NIGHT','RUNID','EventNum'])
    return df

def prepare_df(df):
    #take the timegradslope along the mayor axis
    df['P1Gradient x sign(CosDeltaAlpha)'] = df['timeGradientSlope_x'] * np.sign(df['CosDeltaAlpha'])
    df['Size_over_(Width*Length)'] = df['Size']/ (df['Width'] * df['Length'])
    df['Area'] = df['Width'] * df['Length']
    df['M3Long x sign(CosDeltaAlpha)'] = df['M3Long'] * np.sign(df['CosDeltaAlpha'])
    return df

def drop_nan_and_inf_inplace( df ):
    df.replace([np.inf, -np.inf], np.nan,inplace=True) 
    df.dropna(inplace=True)

def flatten(list_of_lists):
    return [item for sublist in list_of_lists for item in sublist]

def all_files_matching(cfg_entry):
    return flatten([sorted( glob.glob(i) ) for i in cfg_entry])

def estimate_r_XSCATT_assuming_circle(x_core_loc_list,y_core_loc_list):
    rs = np.sqrt(x_core_loc_list*x_core_loc_list + y_core_loc_list*y_core_loc_list)
    return rs.max()

def correct_XSCAT_variable(df):
    """
    it is the radius of the thrown circle on ground
    """
    x_buf = np.vstack(df['core location in cm: (x,y)'].values)[:,0]
    y_buf = np.vstack(df['core location in cm: (x,y)'].values)[:,1]
    if len(x_buf) < 100:
        print('Warning: number of thrown events in run small. XSCATT value poorly estimatable.')
        print('Therefore it is guessed according to the particle id (270m for g / 400m for p and others)')
        if np.any(df['particle id (particle code or A x 100 + Z for nuclei)'] == 1): # PID gamma = 1
            df['XSCATT'] = 27000.0
        else:
            df['XSCATT'] = 40000.0    
    else:
        df['XSCATT'] = estimate_r_XSCATT_assuming_circle(x_buf,y_buf)

    return df


def flatten_lists(df, description=None):
    if not description:
        s = df.iloc[0]
        hierarchy_cols = s.index[[col_index for col_index, type_ in enumerate(map(type, s.values)) if type_ == list]]
        for column in hierarchy_cols:
                df[column+"_x"] = df[column].apply(lambda x: np.take(x,0))
                df[column+"_y"] = df[column].apply(lambda x: np.take(x,1))
        df = df.drop(hierarchy_cols, axis=1)
    else:
        for orig_name in description:
            for i, new_name in enumerate(description[orig_name]):
                df[new_name] = df[orig_name].apply(lambda x: np.take(x, i))
        df = df.drop(description.keys(), axis=1)
    return df

def recast_float64_to_float32(df):
    float64_cols = [c for c in df if df[c].dtype == "float64"]
    df[float64_cols] = df[float64_cols].astype("float32")
    return df
 
def recast_int64_to_int32(df):
    int64_cols = [c for c in df if df[c].dtype == "int64"]
    df[int64_cols] = df[int64_cols].astype("int32")
    return df

def drop_db_columns( df, config ):
    column_list = df.keys()
    for column in column_list:
        if column not in config["run_db"]["columns_to_keep"]:
            df.drop(column, axis=1, inplace=True)
    return df

def add_corsika_run_number_from_source_string(df):
    corsika_run_num99XXX_str = df['@source'][0][-15:-12]
    corsika_run_num999_str = df['@source'][0][-31:-28]
    corsika_run_num_str = corsika_run_num99XXX_str + corsika_run_num999_str
    corsika_run_number = np.zeros(len(df),dtype=int) + int(corsika_run_num_str)
    df['run number']=pd.Series(corsika_run_number)
    return df

