from __future__ import absolute_import, print_function, division
from sympy import mpmath
import numpy as np
import scipy as sp

#this is Max N. li-ma significance implementation from the shift helper
def li_ma_significance(N_on, N_off, alpha=0.2):
    N_on = np.array(N_on, copy=False, ndmin=1)
    N_off = np.array(N_off, copy=False, ndmin=1)

    with np.errstate(divide='ignore', invalid='ignore'):
        p_on = N_on / (N_on + N_off)
        p_off = N_off / (N_on + N_off)

        t1 = N_on * np.log(((1 + alpha) / alpha) * p_on)
        t2 = N_off * np.log((1 + alpha) * p_off)

        ts = (t1 + t2)
        significance = np.sqrt(ts * 2)

    significance[np.isnan(significance)] = 0
    significance[N_on < alpha * N_off] = 0

    return significance

#this gives the cdf of the conditional probability
#to measure a binomial ratio q, given n thrown and r successes
#and common other needed functions 
def check_valid_binomial_input(q,n,r):
    if np.any(np.array(q) > 1) or np.any(np.array(q) < 0):
        return False
    if type(n) is not int or type(r) is not int:
        return False
    if n < 0 or r < 0 or n < r:
        return False
    return True

def pdf_binomial_q(q,n,r):
    if not check_valid_binomial_input(q,n,r):
        raise ValueError('This were no valid q,n, or r for calc. binomial distribution functions')
    #make it vectorized
    buf_vec_function = np.frompyfunc(lambda x: binom.pmf(r,n,x)*(1.+n),1,1)
    buf = buf_vec_function(q)
    return buf

def cdf_binomial_q(q,n,r):
    if not check_valid_binomial_input(q,n,r):
        raise ValueError('This were no valid q,n, or r for calc. binomial distribution functions')
    #make it vectorized for later "mpmath.findroot"
    buf_vec_function = np.frompyfunc(lambda x: mpmath.betainc(1+r,1+n-r,0,x,regularized=True),1,1)
    buf = buf_vec_function(q)
    return buf

def cdf_inv_binomial_q(prob, n, r):
    #good ol' bisect will find you root. for sure.
    b=mpmath.findroot(lambda x: cdf_binomial_q(x,n,r)-prob, [0.0,1.0],solver='bisect',tol=1e-8)
    return b

#this calculated the binomial credible interval on the given measurement
#containing 0.6827% of the posterior probability, assuming flat prior for ratio q
def binomial_credible_interval(n,r):
    if n == 0:
        #return full range of the interval in case it went wrong
        return 0.0,0.5,1.0

    if not check_valid_binomial_input(0.5,n,r):
        print (0.5, n, r)
        raise ValueError('This were no valid q,n, or r for calc. binomial distribution functions')
    
    p1 = 0.0
    p2 = 1.0
    mode = r/n

    #if more than a handful of events, take the limit expression
    try:
        if mode == 0.:
            p1 = 0.
            p2 = cdf_inv_binomial_q(0.6827,n,r)
        elif mode == 1.:
            p2 = 1.
            p1 = cdf_inv_binomial_q(0.3173,n,r)
        else:
            p1 = cdf_inv_binomial_q(0.15865,n,r)
            p2 = cdf_inv_binomial_q(0.84135,n,r)
    except:
        std = np.sqrt(((r+1)*(n-r+1))/((n+2)*(n+2)*(n+3)))
        p1 = mode - std
        p2 = mode + std

    return float(p1),mode,float(p2)

#--------------- this is for linear fitting ------------------
def linear_model_chi2( xi, yi, sigma_yi, a, b ):
    chi = (yi - xi*b - a)/sigma_yi
    return np.sum( chi**2 )

def S(sigma_yi):
    return np.sum( 1/sigma_yi/sigma_yi )

def S_( _, sigma_yi ):
    return np.sum( _/sigma_yi/sigma_yi )

def delta( xi, sigma_yi ):
    return S(sigma_yi)*S_(xi*xi,sigma_yi) - (S_(xi,sigma_yi))**2

def get_a( xi, yi, sigma_yi ):
    #this is the offset of the linear fit
    return (S_(xi*xi,sigma_yi)*S_(yi,sigma_yi)-S_(xi,sigma_yi)*S_(xi*yi,sigma_yi))/delta(xi,sigma_yi)

def get_b( xi, yi, sigma_yi ):
    #this is the slope of the linear fit
    return (S(sigma_yi)*S_(xi*yi,sigma_yi)-S_(xi,sigma_yi)*S_(yi,sigma_yi))/delta(xi,sigma_yi)

def get_da( xi, sigma_yi ):
    #this is the error of the offset of the linear fit
    return S_(xi*xi,sigma_yi)/delta(xi,sigma_yi)

def get_db( xi, sigma_yi ):
    #this is the error of the offset of the linear fit
    return S(sigma_yi)/delta(xi,sigma_yi)

def get_lin_fit( xi, yi, sigma_yi ):
    # y = a + b * x (model)
    a = get_a( xi,yi,sigma_yi )
    b = get_b( xi,yi,sigma_yi )
    da= get_da( xi,sigma_yi )
    db= get_db( xi,sigma_yi )
    chi2 = linear_model_chi2( xi,yi,sigma_yi,a,b)

    #return a, sigma(a), b, sigma(b), chi^2, NDF 
    return a,np.sqrt(da),b,np.sqrt(db),chi2,len(xi)-2