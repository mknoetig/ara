mmcs_default_drop_keys = [
    "AATM", 
    "BATM", 
    "CATM", 
    "CERENKOV Flag (is a bitmap --> usersguide)",
    "CETA", 
    "CKA", 
    "CSTRBA", 
    "CURVED flag (0=standard, 2=CURVED)",
    "Cherenkov output directed to particle output file (= 0.) or Cherenkov output file (= 1.)",
    "DPMJET cross-section flag (0.=no DPMJET, 1.=DPMJET)",
    "DPMJET interaction flag (0.=no DPMJET, 1.=DPMJET)",
    "EFRCTHN energy fraction of thinning level hadronic",
    "EFRCTHN x THINRAT energy fraction of thinning level em-particles",
    "HLAY", 
    "NEUTRINO flag",
    "NFLAIN", 
    "NFLCHE", 
    "NFLCHE + 100 x NFRAGM", 
    "NFLDIF", 
    "NFLPI0",
    "NFLPI0 + 100 x NFLPIF", 
    "NFLPIF", 
    "NFRAGM",
    "NKG radial distribution range in cm",
    "QGSJET X-sect. flag (0.=no QGSJET, 1.=QGSJETOLD,2.=QGSJET01c, 3.=QGSJET-II)",
    "QGSJET interact. flag (0.=no QGSJET, 1.=QGSJETOLD,2.=QGSJET01c, 3.=QGSJET-II)",
    "SIBYLL cross-section flag (0.= no SIBYLL, 1.=vers.1.6; 2.=vers.2.1)",
    "SIBYLL interaction flag (0.= no SIBYLL, 1.=vers.1.6; 2.=vers.2.1)",
    "VENUS/NE X US/EPOS cross-section flag (0=neither, 1.=VENUSSIG,2./3.=NEXUSSIG, 4.=EPOSSIG)",
    "actual weight limit WMAX for thinning hadronic",
    "actual weight limit WMAX x WEITRAT for thinning em-particles",
    "computer flag (3=UNIX, 4=Macintosh)",
    "flag for EGS4 treatment of em. component",
    "flag for NKG treatment of em. component", 
    "flag for activating EGS4",
    "flag for activating NKG",
    "flag for additional muon information on particle output file",
    "flag for hadron origin of electromagnetic subshower on particle tape",
    "flag for observation level curvature (CURVOUT) (0.=flat, 1.=curved)",
    "flag indicating that explicite charm generation is switched on",
    "phyiscal constants",
    "random number sequences: (seed, #calls, #billion calls)",
    "viewing cone VIEWCONE (in deg): (inner, outer) angle",
    "step length factor for multiple scattering step length in EGS4",
    "Cherenkov bandwidth in nm: (lower, upper) end",
    "Cherenkov bunch size in the case of Cherenkov calculations",
    "Earth's magnetic field in uT: (x,z)",
    "X-displacement of inclined observation plane",
    "Y-displacement of inclined observation plane", 
    "Z-displacement of inclined observation plane",
    "altitude (cm) of horizontal shower axis (skimming incidence)",
    "angle (in rad) between array x-direction and magnetic north",
    "core locations for reuse-events in cm: (x, y)",
    "date of begin run", 
    "date of begin run (yymmdd)",
    "energy cutoff for photons in GeV", 
    "grid spacing of Cherenkov detectors in cm (x, y) direction",
    "high-energy hadr. model flag (0.=HDPM,1.=VENUS, 2.=SIBYLL,3.=QGSJET, 4.=DPMJET, 5.=NE X US, 6.=EPOS)",
    "kin. energy cutoff for electrons in GeV",
    "kin. energy cutoff for hadrons in GeV",
    "kin. energy cutoff for muons in GeV",
    "length of each Cherenkov detector in cm in (x, y) direction",
    "low-energy hadr. model flag (1.=GHEISHA, 2.=UrQMD, 3.=FLUKA)",
    "max. radius (in cm) for radial thinning",
    "muon multiple scattering flag (1.=Moliere, 0.=Gauss)",
    "number i of uses of each Cherenkov event",
    "number of Cherenkov detectors in (x, y) direction",
    "phi angle of normal vector of inclined observation plane",
    "phi interval (in degree): (lower, upper edge) ",
    "skimming incidence flag (0.=standard, 1.=skimming)",
    "starting altitude in g/cm2",
    "starting height (cm)",
    "theta angle of normal vector of inclined observation plane",
    "theta interval (in degree): (lower, upper edge) ",
    "transition energy high-energy/low-energy model (in GeV)",
    "version of program",
    "momentum in GeV/c in (x, y, -z) direction;",
    "energy range"
]
