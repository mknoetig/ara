#!/bin/bash
export PATH=/usr/java/jdk1.7.0_10/bin:$PATH

echo "java -jar -Xmx2500M -Xms2500M \
    $jar_path \
    $xml_path \
    -DauxFolder=$auxFolder \
    -Doutfile=$outfile \
    -Dinfile=$infile \
    -Ddrsfile=$drsfile"

java -jar -Xmx2500M -Xms2500M \
    $jar_path \
    $xml_path \
    -DauxFolder=$auxFolder \
    -Doutfile=$outfile \
    -Dinfile=$infile \
    -Ddrsfile=$drsfile
