# How to perform a fact-tools based analysis at ISDC big time :-)

## 1. Get the fact-tools release of your choice

This means, either go to the [fact-tools release page](https://github.com/fact-project/fact-tools/releases) or 
generate your own bleeding edge version of fact-tools. In the end all you need is a jar file.

Copy the jar-file right here next to the `submit.py` file.

## 2. Provide your analysis.xml file

Somehow you need to define, what you want to do with the data. This definition in fact-tools is provided in an XML file.
A typical example might look like this:

    <container>

        <properties url="classpath:/default/settings.properties" />
        <property name="infile" value="file:src/main/resources/testDataFile.fits.gz" />
        <property name="drsfile" value="file:src/main/resources/testDrsFile.drs.fits.gz" />
        <property name="integralGainFile" value="classpath:/default/gain_sorted_20131127.csv" />
        <property name="pixelDelayFile" value="classpath:/default/delays_lightpulser_20150217.csv" />
        <property name="outfile" value="file:/home/fabian/testoutfile.txt" />
        <property name="auxFolder" value="file:src/main/resources/aux/" />
        <service id="auxService" class="fact.auxservice.AuxFileService" auxFolder="${auxFolder}" />
        <service id="calibService" class="fact.calibrationservice.ConstantCalibService" />
        <stream id="fact" class="fact.io.zfits.ZFitsStream"  url="${infile}"/> 

        <process id="2" input="fact">
            <include url="classpath:/default/data/prevEventAndSkip.xml" />
            <include url="classpath:/default/data/calibration.xml" />
            <include url="classpath:/default/data/extraction.xml" />     
            <include url="classpath:/default/data/cleaning.xml" />     
            <include url="classpath:/default/data/parameterCalc.xml" /> 
            <include url="classpath:/default/data/sourceParameter.xml" /> 
            <fact.io.JSONWriter url="${outfile}" keys="${keysForOutput}" writeListOfItems="true" />
        </process>
    </container>
      
Store this definition of your Analysis in a file called e.g. `analysis.xml`

## 3. Choose a script to be called on each workernode

We provide a script for your convenience, which will be called on each worker node. It is called 
`workernode.sh` by default. You can of course write your own and provide it later

## IMPORTANT: test before you submit.

Before submitting several thousand jobs to the cluster, first test (with a single file at least) if your entire setup consisting of:

 * fact-tools jar file
 * analyis xml file
 * workernode shell script

actually works.

In order to do this, the submit script has the `--test` option.

### --test Option

When invoking `submit.py` with the `--test` option, it will create a `workernode_test.sh` for you, which you can call just like:

    bash worker_node.sh

It it works, you are as safe as you can be that it might also work on the workernodes.

Congratulations! You are all set for mass production.

## Example call

Let's assume you have collected the 3 needed files, then you might want to call
`submit.py` like this:

    submit.py --jar=fact-tools.jar --xml=analysis.xml --script=workernode.sh --infiles='/fact/raw/2013/07/*/*'

**Note** put single quotes around the infiles search path expression. Otherwise your shell tries to expant the wildcards.

This call will submit millions and millions of jobs ... depending on the number of `*` in your `--infiles` expression :-D
You have now time to have a lot of tea.

## Results?

While the workernodes are doing their jobs, you can have a look at the status of the output. 

`submit.py` creates 3 folders: `err/`, `out/`, and `json/` in the current directory. 
The out and err folders contain the redirected stdout and stderr streams of the fact-tools process. Normally the stderr files should be empty.
So in order to get a quick overview about the the number of erroneus processes consider:

    find err/ -type f -size +3c | wc -l
    
This should give you the number of files in the err folder, which contain more than 3 bytes (chars).
Skip the `| wc -l` part in order to get the complete list of faulty files.

## JSON -> meh, HDF5 -> YEAH!

In case you'd like to convert all (or part) of these results to a HDF5 file (with a pandas Dataframe inside), consider calling:

    to_hdfstore.py 'json/2014/*/*/*.json' image_parameters_2014.h5

This will also take a lot of time, but shrink down the output about a factor of 8.

## Getting my data out of the HDFStore ?!

Okay so you downloaded this 10GB hdf5 file and want to look at the data, but its too large to even fit in your memory?

(Then your machine has not enough memory :-D)

No seriously, we've added an index to the file, which you can use to query the file like a database. 
Let's say you dowloaded `image_parameters_2016.h5` (11GB) and want to look at a part of it:

    In [1]: import pandas as pd
    
    In [2]: s = pd.HDFStore("image_parameters_2016.h5", "r")
    
    In [3]: s
    Out[3]: 
    <class 'pandas.io.pytables.HDFStore'>
    File path: image_parameters_2016.h5
    /all            frame_table  (typ->appendable,nrows->MANYMANYMANY,ncols->136,indexers->[    index],dc->[UnixTimeUTC])
    
    In [4]: part = s.select("all", where=["UnixTimeUTC < '2016-01-01 19:22:18'",     "UnixTimeUTC > '2016-01-01 19:22:14'"])
    
    In [5]: len(part)
    Out[5]: 44

Please note, you need to provide the Timestamps in the query string inside quotes again... 
So if you use string formating, you'd need to use the `repr` converter `!r`.

For example like this:

    begin = pd.Timestamp("2016.01.01 22:20")
    end = pd.Timestamp("2016.01.01 22:30")
    query = "(UnixTimeUTC > {begin!r}) and (UnixTimeUTC < {end!r})".format(
        begin=begin, 
        end=end)
    part = s.select("all", query)