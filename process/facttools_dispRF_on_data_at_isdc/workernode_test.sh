jar_path=/home/guest/mknoetig/doctorate/programming/ara/process/facttools_dispRF_on_data_at_isdc/fact-tools-0.13.0.jar
xml_path=/home/guest/mknoetig/doctorate/programming/ara/process/facttools_dispRF_on_data_at_isdc/data_processor.xml
auxFolder=file:/fact/aux/2016/01/01/
outfile=file:/home/guest/mknoetig/doctorate/programming/ara/process/facttools_dispRF_on_data_at_isdc/test_result.json
infile=file:/fact/raw/2016/01/01/20160101_011.fits.fz
gpsfile=file:/home/guest/mknoetig/doctorate/data/time_rec_results/out/2016/01/01/20160101_011_trec.txt
tpfile=file:/home/guest/mknoetig/doctorate/data/time_rec_results/out_tp/2016/01/01/20160101_011_tp
drsfile=file:/fact/raw/2016/01/01/20160101_007.drs.fits.gz
#!/bin/bash
export PATH=/usr/java/jdk1.7.0_10/bin:$PATH

echo "java -jar -Xmx2500M -Xms2500M \
    $jar_path \
    $xml_path \
    -DauxFolder=$auxFolder \
    -Doutfile=$outfile \
    -Dinfile=$infile \
    -Dgpsfile=$gpsfile \
    -Dtpfile=$tpfile \
    -Ddrsfile=$drsfile"

java -jar -Xmx2500M -Xms2500M \
    $jar_path \
    $xml_path \
    -DauxFolder=$auxFolder \
    -Doutfile=$outfile \
    -Dinfile=$infile \
    -Dgpsfile=$gpsfile \
    -Dtpfile=$tpfile \
    -Ddrsfile=$drsfile
