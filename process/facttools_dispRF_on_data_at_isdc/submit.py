#!/usr/bin/env python
"""
Facttools job submitter for ISDC

This script will submit fact-tools jobs to worker nodes
at the ISDC computing cluster.

The output of the analysises will by default be put
into the same folder where this script resides.

Best invoke this script on one of the isdc-inxx machines.

In a decent world, you'd need to call this script once
on any host @isdc. But since only isdc-nx00 has access 
to the internet, you need to call this script once, to 
download the jar-file.
And then a seconds time on e.g. isdc-in04 in order to
submit the jobs. Stupid, hu?


Usage:
  submit.py [options] 

Options:
  -h --help          Show this screen.
  --version          Show version.
  --jar PATH         jar path. [default: fact-tools-0.13.0.jar]
  --xml PATH         xml path. [default: data_processor.xml]
  --script PATH      script to call on workernode. [default: workernode.sh]
  --test             test option
  -i --infiles STR   what files should be analysed. [default: /fact/raw/201[5|6]/*/*/*.fits.fz]
  -p --print         just print the commands, instead of submitting
"""
import sys
from docopt import docopt
import glob
import sqlalchemy
import os
import pandas as pd
from tqdm import tqdm
import requests
import time
import shelve
import subprocess as sp
import shlex

from fact_credentials import get_credentials

def get_runinfo():
    if os.path.exists("runinfo.h5"):
        with pd.HDFStore("runinfo.h5") as store:
            runinfo = store["runinfo"]
    else:
        creds = get_credentials()
        spec = 'mysql+pymysql://{user}:{password}@lp-fact/{database}'
        factdb = sqlalchemy.create_engine(spec.format(**creds['database']))
        print("reading complete RunInfo table, takes about 1min.")
        runinfo = pd.read_sql_table("RunInfo", factdb)
        with pd.HDFStore("runinfo.h5") as store:
            store.put("runinfo", runinfo)
    return runinfo

def get_infile_info(infile_path):
    infile_base_path, infile_name = os.path.split(infile_path)
    base_name = infile_name.split('.')[0]
    y,m,d = map(int, (base_name[0:4], base_name[4:6], base_name[6:8]))
    night_int = int(base_name[0:8])
    run = int(base_name.split('_')[-1])

    info = {
        "infile_path": infile_path,
        "infile_base_path": infile_base_path,
        "infile_name": infile_name,
        "base_name": base_name,
        "y": y,
        "m": m,
        "d": d,
        "night_int": night_int,
        "run": run
    }
    return info

def is_data_run(info, runinfo):
    """ check if run is data run. 
        if run cannot be found in runinfo table, returns False.
    """
    this_run_in_runinfo = runinfo[
        (runinfo.fNight==info["night_int"])&
        (runinfo.fRunID==info["run"])
    ]
    return len(this_run_in_runinfo) > 0 and this_run_in_runinfo.iloc[0].fRunTypeKey == 1

def add_drs_run_to_info(info, runinfo):
    drs_run_candidates = runinfo[
        (runinfo.fNight == info["night_int"])&
        (runinfo.fDrsStep == 2)&
        (runinfo.fRunTypeKey==2)&
        (runinfo.fRunID < info["run"])
    ]
    
    if len(drs_run_candidates) >= 1:
        info["drs_run"] = drs_run_candidates.iloc[-1].fRunID
    else:
        info["drs_run"] = None
    return info

def make_dirs(paths):
    for p in paths.values():
        dir = os.path.dirname(p)
        if not os.path.isdir(dir):
            os.makedirs(dir)

def security_check(args):
    if not args["--print"] and os.path.isdir("json"):
        msg = "!! DANGER ZONE !!".center(60, '-') + '\n'
        msg += '\n'
        msg += "I found a json/ folder. If I go on, files in that folder will be overwritten.\n"
        msg += "Do you want me to go on?[yes|no]"

        answer = input(msg)
        while not answer in ["yes", "no"]:
            answer = input(msg+"\n please answer 'yes' or 'no':")

        if answer == "no":
            print("Exiting...")
            sys.exit(0)



if __name__ == "__main__":
    arguments = docopt(__doc__, version='Submitter 1.0')
    print(arguments)
    security_check(arguments)

    path_templates = {
        "jar_path": os.path.abspath(arguments["--jar"]),
        "xml_path": os.path.abspath(arguments["--xml"]),
        "worker_node_script_path": os.path.abspath(arguments["--script"]),
        "json_path": os.path.abspath("json/{y:04d}/{m:02d}/{d:02}/{base_name}.json"),
        "stdout_path": os.path.abspath("out/{y:04d}/{m:02d}/{d:02}/{base_name}.txt"),
        "stderr_path": os.path.abspath("err/{y:04d}/{m:02d}/{d:02}/{base_name}.txt"),
        "drsfile":"{infile_base_path}/{night_int:08d}_{drs_run:03d}.drs.fits.gz",
        "gpsfile_path":"/home/guest/mknoetig/doctorate/data/time_rec_results/out/{y:04d}/{m:02d}/{d:02}/{base_name}_trec.txt",
        "tpfile_path":"/home/guest/mknoetig/doctorate/data/time_rec_results/out_tp/{y:04d}/{m:02d}/{d:02}/{base_name}_tp",
        "aux_dir": "/fact/aux/{y:04d}/{m:02d}/{d:02d}/",
        "infile_path": "{infile_path}",
    }

    runinfo = get_runinfo()
    for infile_path in tqdm(sorted(glob.glob(arguments["--infiles"]))): 
        info = get_infile_info(infile_path)

        if not is_data_run(info, runinfo):
            continue

        info = add_drs_run_to_info(info, runinfo)
        if info["drs_run"] is None:
            continue

        paths = {n:p.format(**info) for (n,p) in path_templates.items()}

        cmd = ("qsub "
                "-q fact_short "
                "-o {stdout_path} "
                "-e {stderr_path} "
                "-v "
                "jar_path={jar_path},"
                "xml_path={xml_path},"
                "auxFolder=file:{aux_dir},"
                "outfile=file:{json_path},"
                "infile=file:{infile_path},"
                "gpsfile=file:{gpsfile_path},"
                "tpfile=file:{tpfile_path},"
                "drsfile=file:{drsfile} "
                "{worker_node_script_path}"
        ).format(**paths)

        if arguments["--test"]:
            with open("workernode_test.sh", "w") as f:
        
                paths["json_path"]=os.path.abspath("test_result.json")
                f.write(("jar_path={jar_path}\n"
                "xml_path={xml_path}\n"
                "auxFolder=file:{aux_dir}\n"
                "outfile=file:{json_path}\n"
                "infile=file:{infile_path}\n"
                "gpsfile=file:{gpsfile_path}\n"
                "tpfile=file:{tpfile_path}\n"
                "drsfile=file:{drsfile}\n").format(**paths))
                f.write(open(paths["worker_node_script_path"], "r").read())
                f.close()
            print("workernode_test.sh written: please type: bash workernode_test.sh")
            break
        elif arguments["--print"]:
            print(cmd)
        else:
            make_dirs(paths)
            sp.check_output(shlex.split(cmd))
