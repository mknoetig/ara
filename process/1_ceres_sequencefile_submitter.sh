#!/bin/sh
 
# A simple script to run ceres
# this script must be executed in the folder it resides
#
# two arguments: $1 must be the base folder of the MC from which 1_corsika, 2_ceres etc branch
#				 $2 is the sequence file for trivial parallelization	
#
# Author: Max Ahnen

# Definitions
CERES="ceres" # ceres command
OPTIONS="-f -b -q --fits" #general options: f:overwrite, b:batch, q:quit after finished

#which mc set to run on
INFILES=$2
OUTPATH=$1"/2_ceres/"
CONFIG="ceres_settings/ceres12.rc"

# Command
echo Start
while read line; do 
	${CERES} ${OPTIONS} --out=${OUTPATH} --config=${CONFIG} $line
done < $INFILES