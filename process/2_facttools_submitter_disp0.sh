#!/bin/sh
 
# A simple script to run facttools
# this script must be executed in the folder it resides
#
# one argument: $1 must be the base folder of the MC from which 1_corsika, 2_ceres etc branch
#
# Author: Max Ahnen

# Definitions
JAVA="java -jar" 
OPTIONS="-Xmx3000M -Xms3000M" 
FACTTOOLS="/home/mknoetig/software/FACTTOOLS/target/fact-tools-0.13.0.jar"
XML="facttools_xml/mc_processor_disp0.xml"

#loop over *Event.fits files in the 2_ceres folder and write output to 3_facttools folder
OUTPATH=$1"/3_facttools_disp0/"
find $1"/2_ceres/" -iname "*Events.fits" | while read INFILE
do
	# Command
	INBASE=`basename ${INFILE} .fits`
	${JAVA} ${OPTIONS} ${FACTTOOLS} ${XML} -Doutfile=file:${OUTPATH}/${INBASE}.json -Dinfile=file:${INFILE}
done

