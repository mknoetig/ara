#!/bin/env python
"""Convert folder of json files into one single HDFStore

Usage:
  to_sqlite3.py <search_ex> <outfilename> 
  to_sqlite3.py (-h | --help)


Options:
  -h --help     Show this screen.
  --version     Show version.
"""
from docopt import docopt
from inspecting import guess_inspector
import os
import glob
import pandas as pd
from tqdm import tqdm
import sys
import time
import sqlite3

import sqlalchemy

if __name__ == '__main__':
    arguments = docopt(__doc__, version='to_sqlite3 0.1')
    connection = sqlalchemy.create_engine("sqlite:///"+arguments["<outfilename>"])

    pathes = glob.glob(os.path.realpath(arguments["<search_ex>"]))

    inspector_class = guess_inspector(pathes[0])
    inspector = inspector_class()

    for path in tqdm(pathes):
        try:
            inspector.to_sqlite3(path, connection)
        except Exception as e:
            pass
    
    connection.execute("create index time_index on observations(time)")
    connection.execute("create index night_run_event_index on observations(NIGHT, RUNID, EventNum)")