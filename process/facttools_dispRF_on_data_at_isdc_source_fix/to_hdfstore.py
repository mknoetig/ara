#!/bin/env python
"""Convert folder of json files into one single HDFStore

Usage:
  to_hdfstore.py <search_ex> <outfilename> 
  to_hdfstore.py (-h | --help)
  to_hdfstore.py --version


Options:
  -h --help     Show this screen.
  --version     Show version.
"""
from docopt import docopt
from inspecting import guess_inspector
import os
import glob
import pandas as pd
from tqdm import tqdm
import sys
import time



if __name__ == '__main__':
    arguments = docopt(__doc__, version='to_hdfstore 0.1')

    store = pd.HDFStore(arguments["<outfilename>"], "w", append=True)
    pathes = glob.glob(os.path.realpath(arguments["<search_ex>"]))

    inspector_class = guess_inspector(pathes[0])
    inspector = inspector_class()

    for path in tqdm(pathes):
        try:
            inspector.to_hdf5(path, store)
        except Exception as e:
            pass
    store.close()
