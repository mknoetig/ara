import glob
from tqdm import tqdm
import json

import numpy as np
import pandas as pd

import py_mmcs_corsika
import mmcs_default_drop_keys


class Inspector:
    default_drop_keys = None
    default_flatten_description = None
    default_rename_cols = None

    def __init__(self, rename_cols=None, drop_keys=None, flatten_description=None):
        if rename_cols is None:
            self.rename_cols = self.default_rename_cols
        else:
            self.rename_cols = rename_cols
        if drop_keys is None:
            self.drop_keys = self.default_drop_keys
        else:
            self.drop_keys = drop_keys
        if flatten_description is None:
            self.flatten_description = self.default_flatten_description
        else:
            self.flatten_description = flatten_description

    def inspect(self, path):
        run = self.open_path_make_dataframe(path)

        run = self.flatten_lists(run)
        run = self.rename_columns(run)
        run = self.do_drop_keys(run)
        run = self.recast_float64_to_float32(run)
        run = self.recast_int64_to_int32(run)
        #run = self.drop_nan_and_inf(run)
        return run

    def open_path_make_dataframe(self, path):
        raise NotImplementedError()

    def rename_columns(self, df):
        if self.rename_cols:
            df = df.rename(columns=self.rename_cols)
        return df

    def do_drop_keys(self, df):
        if self.drop_keys:
            df = df.drop(self.drop_keys, axis=1)
        return df

    def flatten_lists(self, df):
        if not self.flatten_description:
            s = df.iloc[0]
            hierarchy_cols = s.index[[col_index for col_index, type_ in enumerate(map(type, s.values)) if type_ == list]]
            for column in hierarchy_cols:
                    df[column+"_x"] = df[column].apply(lambda x: np.take(x,0))
                    df[column+"_y"] = df[column].apply(lambda x: np.take(x,1))
            df = df.drop(hierarchy_cols, axis=1)
        else:
            for orig_name in self.flatten_description:
                new_names = self.flatten_description[orig_name]
                for i, new_name in enumerate(new_names):
                    df[new_name] = df[orig_name].apply(lambda x: np.take(x, i))
            df = df.drop(self.flatten_description.keys(), axis=1)
        return df

    def recast_float64_to_float32(self, df):
        float64_cols = [c for c in df if df[c].dtype == "float64"]
        df[float64_cols] = df[float64_cols].astype("float32")
        return df
     
    def recast_int64_to_int32(self, df):
        int64_cols = [c for c in df if df[c].dtype == "int64"]
        df[int64_cols] = df[int64_cols].astype("int32")
        return df

    def drop_nan_and_inf(self, df):
        df = df.replace([np.inf, -np.inf], np.nan) 
        df = df.dropna()
        return df

class CherenkovPhotonInspector(Inspector):
    default_drop_keys = mmcs_default_drop_keys.mmcs_default_drop_keys
    default_flatten_description = {
        "angle in radian: (zenith, azimuth)" : [
            "zenith angle in radian", 
            "azimuth angle in radian"],
        "core location in cm: (x,y)": [
            "x core location in cm", 
            "y core location in cm"],
        "observation levels": [
            "observation level"]
    }
    default_rename_cols = {
        "run number": "run",
        "event number": "event",
        "reuse number": "reuse",
        "number of first target if fixed": "first_target_number",
        "particle id (particle code or A x 100 + Z for nuclei)": "primary",
        "slope of energy spektrum": "energy_slope",
        "total energy in GeV": "total_energy_GeV",
        "z coordinate (height) of first interaction in cm": "first_interaction_height_cm",
        "observation level": "observation_level",
        "x core location in cm": "x_core_cm",
        "y core location in cm": "y_core_cm",
        "zenith angle in radian": "zenith_radian",
            "azimuth angle in radian": "azimuth_radian"
    }


    def open_path_make_dataframe(self, path):
        event_getter = py_mmcs_corsika.EventGetter(path)
        run = pd.DataFrame([{**event_getter.RunHeader, **corsika_event.Header} for corsika_event in event_getter])   
        run = self.correct_XSCAT_variable(run)
        run["run number"] = run["run number"].astype(np.int32)
        return run

    def correct_XSCAT_variable(self, df):
        """
        it is the radius of the thrown circle on ground
        """
        x_buf = np.vstack(df['core location in cm: (x,y)'].values)[:,0]
        y_buf = np.vstack(df['core location in cm: (x,y)'].values)[:,1]
        if len(x_buf) < 100:
            print('Warning: number of thrown events in run small. XSCATT value poorly estimatable.')
            print('Therefore it is guessed according to the particle id (270m for g / 400m for p and others)')
            if np.any(mc_data_frame['particle id (particle code or A x 100 + Z for nuclei)'] == 1): # PID gamma = 1
                df['XSCATT'] = 27000.0
            else:
                df['XSCATT'] = 40000.0    
        else:
            df['XSCATT'] = self.estimate_r_XSCATT_assuming_circle(x_buf, y_buf)

        df['XSCATT'] /= 100 # set unit to meters
        return df


    def estimate_r_XSCATT_assuming_circle(self, x_core_loc_list, y_core_loc_list):
        rs = np.sqrt(x_core_loc_list*x_core_loc_list + y_core_loc_list*y_core_loc_list)
        return rs.max()

    def to_hdf5(self, path, store):
        run = self.inspect(path)
        store.append(
            "cherenkov_photons",
            run,
            data_columns=[
                "run", "event", "reuse",
                "primary",
                "total_energy_GeV",
            ],
            index=False
        )

    def to_sqlite3(self, path, connection):    
        run = self.inspect(path)    
        run.to_sql("cherenkov_photons", connection, if_exists='append')

    
class ShowerParameterInspector(Inspector):
    default_rename_cols = {
        "MMcEvt.fEvtNumber": "event",
        "MMcEvt.fEventReuse": "reuse",
        "CorsikaEvtHeader.fFirstInteractionHeight": "truth_FirstInteractionHeight",
        "MCorsikaEvtHeader.fTotalEnergy": "truth_TotalEnergy",
        "MCorsikaEvtHeader.fMomentumX": "truth_Momentum_x",
        "MCorsikaEvtHeader.fMomentumY": "truth_Momentum_y",
        "MCorsikaEvtHeader.fMomentumZ": "truth_Momentum_z",
        "MCorsikaEvtHeader.fX": "truth_Position_x",
        "MCorsikaEvtHeader.fY": "truth_Position_y",
        "MCorsikaEvtHeader.fAz": "truth_Az",
        "MCorsikaEvtHeader.fZd": "truth_Zd",
        "MCorsikaEvtHeader.fWeightedNumPhotons": "truth_WeightedNumPhotons",
    }
    default_drop_keys = [
            "EventNum",
            "NPIX",
            "NROI",
    ]



    def open_path_make_dataframe(self, path):
        with open(path) as f:
            run = pd.DataFrame(json.load(f))
        run = self.add_corsika_run_number_from_source_string(run)
        return run

    def add_corsika_run_number_from_source_string(self, df):
        corsika_run_num99XXX_str = df['@source'][0][-15:-12]
        corsika_run_num999_str = df['@source'][0][-31:-28]
        corsika_run_num_str = corsika_run_num99XXX_str + corsika_run_num999_str
        corsika_run_number = np.zeros(len(df), dtype=np.int32) + int(corsika_run_num_str)
        df['run'] = pd.Series(corsika_run_number)
        df.drop('@source', axis=1, inplace=True)
        return df

    def inspect(self, path):
        run = super().inspect(path)
        return self.prepare_df(run)

    def prepare_df(self, df):
        #take the timegradslope along the mayor axis
        df['P1Gradient x sign(CosDeltaAlpha)'] = df['timeGradientSlope_x'] * np.sign(df['CosDeltaAlpha'])
        df['Size_over_(Width*Length)'] = df['Size']/ (df['Width'] * df['Length'])
        df['Area'] = df['Width'] * df['Length']
        df['M3Long x sign(CosDeltaAlpha)'] = df['M3Long'] * np.sign(df['CosDeltaAlpha'])
        return df

    def to_hdf5(self, path, store):
        store.append("simulations", 
            self.inspect(path),
            data_columns=["run", "event", "reuse"],
            format='table',
            index=False
        )

    def to_sqlite3(self, path, connection):    
        run = self.inspect(path)    
        run.to_sql("simulations", connection, if_exists='append')


class ShowerParameterInspectorData(Inspector):
    def open_path_make_dataframe(self, path):
        with open(path) as f:
            run = pd.DataFrame(json.load(f))
        return run

    def to_hdf5(self, path, store):
        store.append(
            "observations", 
            self.inspect(p),
            data_columns=["time"],
            format='table',
            index=False
        )

    def to_sqlite3(self, path, connection):    
        run = self.inspect(path)    
        run.to_sql("observations", connection, if_exists='append')


def guess_inspector(path):
    for inspector in [
        CherenkovPhotonInspector,
        ShowerParameterInspector,
        ShowerParameterInspectorData
    ]:
        try:
            inspector().inspect(path)
            return inspector
        except Exception as e:
            pass


def get_evts(path, inspector):
    df_list = []
    for path in tqdm(all_files_matching(path)):
        df_list.append(inspector.inspect(path))
    df = pd.concat(df_list, ignore_index=True)
    df = df.set_index(['run number','event number','reuse number'])
    return df


def all_files_matching(cfg_entry):
    return flatten([sorted( glob.glob(i) ) for i in cfg_entry])


def flatten(list_of_lists):
    return [item for sublist in list_of_lists for item in sublist]



