jar_path=/home/guest/mknoetig/doctorate/programming/ara/process/facttools_on_data_at_isdc_source_fix/fact-tools-0.13.3.jar
xml_path=/home/guest/mknoetig/doctorate/programming/ara/process/facttools_on_data_at_isdc_source_fix/analysis.xml
auxFolder=file:/fact/aux/2016/01/01/
outfile=file:/home/guest/mknoetig/doctorate/programming/ara/process/facttools_on_data_at_isdc_source_fix/test_result.json
infile=file:/fact/raw/2016/01/01/20160101_011.fits.fz
drsfile=file:/fact/raw/2016/01/01/20160101_007.drs.fits.gz
#!/bin/bash
export PATH=/usr/java/jdk1.7.0_10/bin:$PATH

echo "java -jar -Xmx2500M -Xms2500M \
    $jar_path \
    $xml_path \
    -DauxFolder=$auxFolder \
    -Doutfile=$outfile \
    -Dinfile=$infile \
    -Ddrsfile=$drsfile"

java -jar -Xmx2500M -Xms2500M \
    $jar_path \
    $xml_path \
    -DauxFolder=$auxFolder \
    -Doutfile=$outfile \
    -Dinfile=$infile \
    -Ddrsfile=$drsfile
