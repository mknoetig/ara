#!/bin/sh
 
# A simple script to run ceres
# this script must be executed in the folder it resides
#
# one argument: $1 must be the base folder of the MC from which 1_corsika, 2_ceres etc branch
#
# Author: Max Ahnen

# Definitions
CERES="ceres" # ceres command
OPTIONS="-f -b -q --fits" #general options: f:overwrite, b:batch, q:quit after finished

#which mc set to run on
INFILES=$1"/1_corsika/cer*"
OUTPATH=$1"/2_ceres/"
CONFIG="ceres_settings/ceres12.rc"

# Command
${CERES} ${OPTIONS} --out=${OUTPATH} --config=${CONFIG} ${INFILES} 