""" high level plotting program, creates theta^2 plot and spectra 
    for a given timerange, source, etc..

Usage:
  high_level_plots.py [--noshow] <high_level_settings>
  high_level_plots.py (-h | --help)
  high_level_plots.py --version

Options:
  -h --help     Show this screen.
  --version     Show version.
"""
from __future__ import absolute_import, print_function, division

import os
import json
import sys
import pickle

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

from tqdm import tqdm
from docopt import docopt

from hdf_storage import get_events_df_from_hdf5
import inspecting
import csv
import training_and_test as T_T
import plotting
import statistics
from sources import *

# ----- the start of the main -----------
if __name__ == "__main__":

    #read models and initialize program
    arguments = docopt(__doc__, version='Ara 1')
    noshow = arguments["--noshow"]
    high_level_settings = inspecting.read_config_file(arguments["<high_level_settings>"])
 
    print('reading models file...')
    with open(high_level_settings["models_file"], 'rb') as handle:
        models_file = pickle.load(handle)

    energy_models = models_file['energy_models']
    classifier_models = models_file['classifier_models']
    Aeff_buf = models_file['Aeff']
    Aeff = plotting.fix_aeff_version_problem(Aeff_buf)

    configuration = models_file['configuration']

    #to cut away crap
    minimum_pid_ness = 0.5
    maximum_theta = 170.65

    #get the data from the storage
    print('reading the stored data...')
    store           = pd.HDFStore(high_level_settings["high_level_data_file"])
    query_time_low  = "NIGHT>=%s"%high_level_settings["fNight_min"]
    query_time_high = "NIGHT<=%s"%high_level_settings["fNight_max"]
    query_source    = "fSourceKEY=%s"%source_map[high_level_settings["source"]]
    query_zenith_low= "fZenithDistanceMean>=%s"%high_level_settings["zenith_min"]
    query_zenith_high= "fZenithDistanceMean<=%s"%high_level_settings["zenith_max"]
    query_pid_ness  = "ARA_pid_1_to_14_ness >=%s"%str(minimum_pid_ness)
    query_threshold = "fThresholdMedian<=%s"%str(high_level_settings["threshold_max"])
    query_theta     = "Theta<=%s"%str(maximum_theta) # this is in order to kick out too far away events

    data_evts_raw=store.select('evts',[ 
        query_time_low,
        query_time_high,
        query_source,
        query_zenith_low,
        query_zenith_high,
        query_pid_ness,
        query_theta
        ])
    print('done.')

    #apply the default mc missmatch cuts
    print("applying default mc missmatch cuts")
    data_evts = T_T.apply_default_mc_missmatch_cuts( data_evts_raw, configuration )

    #get the magic data
    print("reading magic data and night exclusion file")
    magic_file=open( high_level_settings["magic_data"], "r")
    magic_data=[]
    reader = csv.reader(magic_file)
    for line in reader:
        if line[0][0]=="#":
            continue
        magic_data.append([int(line[0]),int(line[1]),int(line[2]),float(line[3])])
    magic_file.close()
    
    #make the magic data a pd.df
    magic_data_df = pd.DataFrame(magic_data)
    magic_data_df.columns = ['NIGHT', 'RUNID','EventNum','Magic_Energy']
    magic_data_df = magic_data_df.set_index(['NIGHT','RUNID','EventNum'])
    
    #join the missing data from the fact data frame
    magic_data_df = pd.merge(
        data_evts.reset_index(), 
        magic_data_df.reset_index(), 
        on=['NIGHT','RUNID','EventNum'], 
        how='inner')
    magic_data_df = magic_data_df.set_index(['NIGHT','RUNID','EventNum'])

    #get the night selection from exclusion lists
    nights_exclusion_file = open(high_level_settings["data_exclusions_file"], 'rb')
    nights_exclusion_data = pickle.load(nights_exclusion_file)
    nights_exclusion_file.close()

    #make a list of possible dataruns to be excluded
    #list_of_nights = [int(i) for i in list(nights_exclusion_data.keys())]
    nights_exclusion_df = []
    for NIGHT in list(nights_exclusion_data.keys()):
        runs_to_be_added = []
    
        run_edges = nights_exclusion_data[NIGHT]
        run_edges_numpy = (np.array(run_edges)).reshape((-1,2)) #reshape in order to iterate over it later easily
        
        for edge_tupel in run_edges_numpy:
            runs_to_be_added.extend(list(range(edge_tupel[0],edge_tupel[1]+1)))
    
        for run in runs_to_be_added:
            nights_exclusion_df.append([int(NIGHT),run])

    nights_exclusion_df=pd.DataFrame(nights_exclusion_df)
    nights_exclusion_df.columns = ['NIGHT', 'RUNID']
    nights_exclusion_df["bad_quality"]=True
    nights_exclusion_df = nights_exclusion_df.set_index(['NIGHT','RUNID'])

    #add a column to the data with the selection joined.
    data_evts=pd.merge(data_evts.reset_index(),nights_exclusion_df.reset_index(), on=['NIGHT','RUNID'], how='left').set_index(['NIGHT','RUNID','EventNum'])
    data_evts.fillna(False, inplace=True)

    #start plotting
    print("start plotting")
    #make the out dir if it does not exist
    if not os.path.exists(high_level_settings["out_folder"]):
        os.makedirs(high_level_settings["out_folder"])
    #make the plots out dir if it does not exist
    if not os.path.exists(high_level_settings["out_folder"]+'/high_level_plots/'):
        os.makedirs(high_level_settings["out_folder"]+'/high_level_plots/')

    #--------------- start plotting -------------
    if not noshow:
        plt.ion()

    '''fig = plt.figure(figsize=(7,9))
    ax1 = fig.add_subplot(211)
    hist_pars = plotting.plot_Migration_matrix_comparison_magic(magic_data_df)
    plt.title('ARA/Magic Energy Comparison')

    ax2 = fig.add_subplot(212,sharex=ax1)
    plotting.plot_energy_resolution_comparison_magic( ax2,magic_data_df,hist_pars )
    plotting.plot_energy_error_comparison_magic( ax2,magic_data_df,hist_pars )
    plt.ylabel(("rel. Energy error pid %d" % 1), fontsize=12)
    plt.xlabel("log10(Magic Energy/GeV)", fontsize=12)
    
    plt.tight_layout()
    plt.savefig(high_level_settings["out_folder"]+'/high_level_plots/magic_energy_comp.png', bbox_inches='tight')
    plt.savefig(high_level_settings["out_folder"]+'/high_level_plots/magic_energy_comp.pdf', bbox_inches='tight')

    fig6 = plt.figure( figsize=(14,14) )
    fig6.add_subplot(331)
    plotting.plot_magic_comparison(magic_data_df,
                                     "Size", 
                                     logy=True,log10=True,loc='lower left',xlim=[1,7])
    fig6.add_subplot(332)
    plotting.plot_magic_comparison(magic_data_df,
                                     "Width", 
                                     logy=False,log10=False,loc='upper right',xlim=[0,45])
    fig6.add_subplot(333)
    plotting.plot_magic_comparison(magic_data_df, 
                                     "Length", 
                                     logy=False,log10=False,loc='upper right',xlim=[0,60])
    fig6.add_subplot(334)
    plotting.plot_magic_comparison(magic_data_df, 
                                     "ARA_pid_1_stdev_Energy_Prediction_in_GeV", 
                                     logy=False,log10=True,xlim=[1.5,4.5])
    fig6.add_subplot(335)
    plotting.plot_magic_comparison(magic_data_df, 
                                     "Disp", 
                                     logy=False,log10=False,xlim=[0,200])
    fig6.add_subplot(336)
    plotting.plot_magic_comparison(magic_data_df,
                                     "Size_over_(Width*Length)", 
                                     logy=True,log10=True,loc='lower left',xlim=[-1.5,3.0])
    fig6.add_subplot(337)
    plotting.plot_magic_comparison(magic_data_df,
                                     "Area", 
                                     logy=False,log10=False,loc='upper right',xlim=[0,3000])
    fig6.add_subplot(338)
    plotting.plot_magic_comparison(magic_data_df,
                                     "Concentration_onePixel", 
                                     logy=True,log10=False,loc='upper right',xlim=[0.0,0.25])
    fig6.add_subplot(339)
    plotting.plot_magic_comparison(magic_data_df, 
                                     "ConcCore", 
                                     logy=False,log10=False,loc='lower left',xlim=[0.0,0.9])
    plt.tight_layout()
    plt.savefig(high_level_settings["out_folder"]+'/high_level_plots/magic_parameter_comp.png', bbox_inches='tight')
    plt.savefig(high_level_settings["out_folder"]+'/high_level_plots/magic_parameter_comp.pdf', bbox_inches='tight')
    '''

    fig7 = plt.figure()
    Non, Noff, Nex, SLiMa, ontime, beg, end = plotting.theta_2_histo_data(
        data_evts[data_evts.bad_quality==False],
        source=high_level_settings["source"],
        gamma_cut=configuration['analysis_cuts']['gamma_proton_ness_cut'],
        theta2_cut=configuration['analysis_cuts']['theta_2_cut'],
        #theta2_cut=0.022743831777778473,
        plotting=True)
    plt.tight_layout()
    print("Theta^2 Analysis")
    print("Non Noff Nex SLiMa ontime/h")
    print(Non, Noff, Nex, SLiMa, ontime/3600.)
    plt.savefig(high_level_settings["out_folder"]+'/high_level_plots/'+high_level_settings["source"]+'_theta2.png', bbox_inches='tight')
    plt.savefig(high_level_settings["out_folder"]+'/high_level_plots/'+high_level_settings["source"]+'_theta2.pdf', bbox_inches='tight')

    pulsar_analysis_energy_range = high_level_settings["pulsar_analysis_energy_range"]
    Non, Noff, alpha_phaso, Nex, SLiMa, ontime =plotting.phasogramm_data(
        data_evts[data_evts.bad_quality==False], 
        source=high_level_settings["source"],
        gamma_cut=configuration['analysis_cuts']['gamma_proton_ness_cut'],
        theta2_cut=configuration['analysis_cuts']['theta_2_cut'],
        #theta2_cut=0.022743831777778473,
        plotting=True,
        energy_cut=pulsar_analysis_energy_range
        )

    print("Pulsar Analysis")
    print("Non Noff Nex SLiMa ontime/h")
    print(Non, Noff, Nex, SLiMa, ontime/3600.)
    plt.savefig(high_level_settings["out_folder"]+'/high_level_plots/'+high_level_settings["source"]+'_phase_analysis.png', bbox_inches='tight')
    plt.savefig(high_level_settings["out_folder"]+'/high_level_plots/'+high_level_settings["source"]+'_phase_analysis.pdf', bbox_inches='tight')

    spectrum_energy_range = high_level_settings["plot_spectrum_range"]
    fig9 = plt.figure()
    plotting.plot_measured_crab_spectra([spectrum_energy_range[0]*0.9,spectrum_energy_range[1]*1.1],all=False)
    plotting.plot_spectrum(
        data_evts[data_evts.bad_quality==False],
        Aeff,
        source=high_level_settings["source"],
        gamma_cut=configuration['analysis_cuts']['gamma_proton_ness_cut'],
        theta2_cut=configuration['analysis_cuts']['theta_2_cut'],
        #theta2_cut=0.022743831777778473,
        beta=0.5,
        energy_range=spectrum_energy_range,
        nmax=high_level_settings["nmax_bins_in_spectrum"]
        )
    #plotting.plot_power_law( 2.590e-11, -3.997, xlim=[550, 1900] )
    plt.legend()
    plt.tight_layout()
    plt.savefig(high_level_settings["out_folder"]+'/high_level_plots/'+high_level_settings["source"]+'_spectrum.png', bbox_inches='tight')
    plt.savefig(high_level_settings["out_folder"]+'/high_level_plots/'+high_level_settings["source"]+'_spectrum.pdf', bbox_inches='tight')


    lambda_threshold = float(input("Enter the lambda threshold for N_on=%d, N_off=%d, alpha=%f: "%(Non, Noff, alpha_phaso)))
    c_lambda = lambda_threshold / np.sqrt(ontime/3600.)

    fig10_1 = plt.figure()
    samples = plotting.plot_crab_pulsar_nebula_comparison()
    
    plotting.plot_integral_limit_slope_independent_sed(Aeff,ton=ontime/3600.,E0=150,integral_energy_range=pulsar_analysis_energy_range,lambda_95=lambda_threshold,linestyle='k')
    plotting.plot_integral_limit_slope_independent_sed(Aeff,ton=ontime*2./3600.,E0=150,integral_energy_range=pulsar_analysis_energy_range,lambda_95=lambda_threshold*np.sqrt(2.),linestyle='k:',print_time=False)
    plotting.plot_integral_limit_slope_independent_sed(Aeff,ton=ontime*4./3600.,E0=150,integral_energy_range=pulsar_analysis_energy_range,lambda_95=lambda_threshold*np.sqrt(4.),linestyle='k:',print_time=False)
    plotting.plot_integral_limit_slope_independent_sed(Aeff,ton=ontime*8./3600.,E0=150,integral_energy_range=pulsar_analysis_energy_range,lambda_95=lambda_threshold*np.sqrt(8.),linestyle='k:',print_time=False)

    plt.savefig(high_level_settings["out_folder"]+'/high_level_plots/crab_pulsar_nebula_comparison_with_ul.png', bbox_inches='tight')
    plt.savefig(high_level_settings["out_folder"]+'/high_level_plots/crab_pulsar_nebula_comparison_with_ul.pdf', bbox_inches='tight')

    fig10_2 = plt.figure()
    samples = plotting.plot_crab_pulsar_nebula_comparison()
    plt.savefig(high_level_settings["out_folder"]+'/high_level_plots/crab_pulsar_nebula_comparison.png', bbox_inches='tight')
    plt.savefig(high_level_settings["out_folder"]+'/high_level_plots/crab_pulsar_nebula_comparison.pdf', bbox_inches='tight')

    fig11 = plt.figure()
    plotting.plot_lines_of_constant_measured_counts(Aeff=Aeff,threshold=lambda_threshold,ton=ontime/3600.,energy_range=pulsar_analysis_energy_range)
    ax = plt.gca()
    plotting.plot_contours_from_sample(samples,axis=ax)
    plt.savefig(high_level_settings["out_folder"]+'/high_level_plots/constant_measured_counts.png', bbox_inches='tight')
    plt.savefig(high_level_settings["out_folder"]+'/high_level_plots/constant_measured_counts.pdf', bbox_inches='tight')

    fig12 = plt.figure()
    plotting.plot_lines_of_detection_threshold(Aeff=Aeff,threshold=lambda_threshold,ton=ontime/3600.,energy_range=pulsar_analysis_energy_range)
    ax = plt.gca()
    plotting.plot_contours_from_sample(samples,axis=ax)
    plt.savefig(high_level_settings["out_folder"]+'/high_level_plots/detection_threshold_over_time.png', bbox_inches='tight')
    plt.savefig(high_level_settings["out_folder"]+'/high_level_plots/detection_threshold_over_time.pdf', bbox_inches='tight')

    fig13 = plt.figure()
    plotting.plot_contours_from_sample(samples)
    plt.xlabel('$f_0$ / [(cm$^2$ s TeV)$^{-1}$]')
    plt.ylabel('$\\alpha$')
    plt.savefig(high_level_settings["out_folder"]+'/high_level_plots/emcee_fit_result.pdf', bbox_inches='tight')    
    plt.savefig(high_level_settings["out_folder"]+'/high_level_plots/emcee_fit_result.png', bbox_inches='tight')    

    if not noshow:
        plt.show()
